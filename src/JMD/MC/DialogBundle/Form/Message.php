<?php

namespace JMD\MC\DialogBundle\Form;

use JMD\MC\CoreBundle\Form\Type\DocumentHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Message extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextareaType::class, [
                'label' => 'dialog.form.new.message',
                'attr'  => [
                    'rows'  => 10
                ]
            ])
            ->add('dialog', DocumentHiddenType::class, [
                'class' => 'JMD\MC\DialogBundle\Document\Dialog',
                'data_class' => null,
                'data' => $options['value']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'translation_domain'    => 'dialog',
            'value'                 => null
        ]);
    }
}
