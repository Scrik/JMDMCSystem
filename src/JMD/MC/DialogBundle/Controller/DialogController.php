<?php
namespace JMD\MC\DialogBundle\Controller;

use JMD\MC\CoreBundle\Controller\BaseController;
use JMD\MC\CoreBundle\Document\Notification;
use JMD\MC\DialogBundle\Document\Dialog as DialogDocument;
use JMD\MC\DialogBundle\Document\Message;
use JMD\MC\DialogBundle\Form\Dialog;
use JMD\MC\DialogBundle\Form\Message as MessageForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DialogController
 * @package JMD\MC\DialogBundle\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 */
class DialogController extends BaseController
{
    public function indexAction()
    {
        $dialogs = $this
            ->getODM()
            ->getRepository('JMDMCDialogBundle:Dialog')
            ->createQueryBuilder()
            ->field('users')
            ->includesReferenceTo($this->getUser())
            ->getQuery()->execute()
        ;

        return $this->render('list.html.twig', [
            'current'   => 'dialog',
            'dialogs'   => $dialogs,
            'title'     => $this->trans('title.dialog.list'),
        ]);
    }

    public function newAction(Request $request)
    {
        $form = $this->createForm(Dialog::class);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $dialog = new DialogDocument();
                $dialog
                    ->setTheme($data['theme'])
                    ->setCreator($this->getUser())
                    ->addUser($this->getUser())
                ;
                if (count($data['users']) == 0) {
                    $this->addFlash('danger', $this->trans('dialog.form.errors.no_users', [], 'dialog'));

                    return $this->render('new.html.twig', [
                        'form' => $form->createView(),
                        'title' => $this->trans('title.dialog.new')
                    ]);
                }

                $users = [];
                foreach ($data['users'] as $user) {
                    if ($user->isInBlackList($this->getUser())) {
                        $this->addFlash('danger', $this->trans('dialog.form.errors.you_are_blacklisted', [
                            '%user%' => $user->getUsername()
                        ], 'dialog'));

                        return $this->render('new.html.twig', [
                            'form' => $form->createView(),
                            'title' => $this->trans('title.dialog.new')
                        ]);
                    }
                    $dialog->addUser($user);
                    $users[] = $user->getUsername();


                    $notification = new Notification();
                    $notification
                        ->setDestination($dialog)
                        ->setRouteName('dialog_view')
                        ->setUser($user)
                        ->setMessage($this->trans('dialog.new_message', [
                            '%theme%' => $dialog->getTheme()
                        ], 'notifications'));
                }

                $this->getODM()->persist($dialog);

                $message = new Message();
                $message
                    ->setUser($this->getUser())
                    ->setDialog($dialog)
                    ->setMessage($data['message'])
                ;

                $this->getODM()->persist($message);

                $notification->setSource($message);

                $this->getODM()->persist($notification);

                $this->getODM()->flush();

                $this->addFlash('success', $this->trans('dialog.form.success.created', [
                    '%users%' => implode(', ', $users)
                ], 'dialog'));

                return $this->redirectToRoute('dialog_list');
            }
        }

        return $this->render('new.html.twig', [
            'form'  => $form->createView(),
            'title' => $this->trans('title.dialog.new')
        ]);
    }

    public function viewAction($id, Request $request)
    {
        $dialog = $this->getODM()->getRepository('JMDMCDialogBundle:Dialog')->find($id);

        $nqb = $this->getODM()->getRepository('JMDMCCoreBundle:Notification')->createQueryBuilder();
        $dialogNotifications = $nqb
            ->addAnd($nqb->expr()->field('user')->references($this->getUser()))
            ->addAnd($nqb->expr()->field('destination')->references($dialog))
            ->getQuery()
            ->execute()
        ;

        foreach ($dialogNotifications as $dialogNotification) {
            $this->getODM()->remove($dialogNotification);
        }
        $this->getODM()->flush();

        if ($dialog === null || !in_array($this->getUser(), $dialog->getUsers()->toArray())) {
            $this->addFlash('danger', $this->trans('dialog.view.error', [], 'dialog'));

            return $this->redirectToRoute('dialog_list');
        }

        $message = new Message();

        $form = $this->createForm(MessageForm::class, $message, ['value' => $dialog]);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $message->setUser($this->getUser());

                foreach ($dialog->getUsers() as $user) {
                    if ($user !== $this->getUser()) {
                        $notification = new Notification();
                        $notification
                            ->setDestination($dialog)
                            ->setRouteName('dialog_view')
                            ->setUser($user)
                            ->setMessage($this->trans('dialog.new_message', [
                                '%theme%' => $dialog->getTheme()
                            ], 'notifications'))
                            ->setSource($message);

                        $this->getODM()->persist($notification);
                    }
                }

                $this->getODM()->persist($message);
                $this->getODM()->flush();

                $this->addFlash('success', $this->trans('dialog.form.success.sended', [], 'dialog'));

                return $this->redirectToRoute('dialog_view', [
                    'id'    => $request->get('id')
                ]);
            }
        }

        return $this->render('view.html.twig', [
            'current'   => 'dialog',
            'form'      => $form->createView(),
            'dialog'    => $dialog,
            'title' => $this->trans('title.dialog.view', [
                '%theme%'   => $dialog->getTheme()
            ])
        ]);
    }

    public function leaveAction($id)
    {
        $dialog = $this->getODM()->getRepository('JMDMCDialogBundle:Dialog')->find($id);

        if ($dialog === null || !in_array($this->getUser(), $dialog->getUsers()->toArray())) {
            $this->addFlash('danger', $this->trans('dialog.view.error', [], 'dialog'));

            return $this->redirectToRoute('dialog_list');
        }
        
        if ($dialog->getCreator() === $this->getUser()) {

            $this->addFlash('danger', $this->trans('dialog.view.leave_error', [], 'dialog'));

            return $this->redirectToRoute('dialog_view', [ 'id' => $id ]);
        }

        $dialog->removeUser($this->getUser());
        $this->getODM()->persist($dialog);
        $this->getODM()->flush();

        $this->addFlash('info', $this->trans('dialog.view.leave', ['%theme%' => $dialog->getTheme()], 'dialog'));

        return $this->redirectToRoute('dialog_list');
    }
}
