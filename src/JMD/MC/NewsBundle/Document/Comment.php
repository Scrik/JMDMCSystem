<?php
namespace JMD\MC\NewsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use JMD\MC\CoreBundle\Document\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Comment
 * @package JMD\MC\NewsBundle\Document
 * 
 * @ODM\Document()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Comment
{
    use TimestampableDocument;
    use SoftDeleteableDocument;

    /**
     * @var string
     * @ODM\Id()
     */
    private $id;

    /**
     * @var News
     * @ODM\ReferenceOne(targetDocument="News", inversedBy="comments")
     * @Assert\NotNull()
     */
    private $news;

    /**
     * @var User
     * @ODM\ReferenceOne(targetDocument="JMD\MC\CoreBundle\Document\User")
     */
    private $user;

    /**
     * @var string
     * @ODM\Field(type="string")
     * @Assert\Length(max="1500")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $content;

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set news
     *
     * @param News $news
     * @return self
     */
    public function setNews(News $news)
    {
        $this->news = $news;
        return $this;
    }

    /**
     * Get news
     *
     * @return News $news
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Get content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }
}
