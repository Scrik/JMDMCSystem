<?php
namespace JMD\MC\NewsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use JMD\MC\CoreBundle\Document\FileStorage;
use JMD\MC\CoreBundle\Document\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class News
 * @package JMD\MC\NewsBundle\Document
 *
 * @ODM\Document()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class News
{
    use TimestampableDocument;
    use SoftDeleteableDocument;

    /**
     * @var string
     * @ODM\Id()
     */
    private $id;

    /**
     * @var array
     * @ODM\Collection()
     */
    private $tags = [];

    /**
     * @var string
     * @ODM\Field()
     * @Assert\Length(max="255")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     * @ODM\Field()
     * @Assert\Length(max="1500")
     */
    private $shortContent;

    /**
     * @var string
     * @ODM\Field()
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $fullContent;

    /**
     * @var User
     * @ODM\ReferenceOne(targetDocument="JMD\MC\CoreBundle\Document\User")
     */
    private $user;

    /**
     * @var Comment
     * @ODM\ReferenceMany(targetDocument="Comment", mappedBy="news", sort={"createdAt"="desc"})
     */
    private $comments;

    /**
     * @var FileStorage[]
     * @ODM\ReferenceMany(targetDocument="JMD\MC\CoreBundle\Document\FileStorage", mappedBy="document")
     */
    private $files;

    /**
     * @var boolean
     * @ODM\Field(type="boolean")
     */
    private $enabled = false;
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tags
     *
     * @param array $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * Get tags
     *
     * @return array $tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set shortContent
     *
     * @param string $shortContent
     * @return self
     */
    public function setShortContent($shortContent)
    {
        $this->shortContent = $shortContent;
        return $this;
    }

    /**
     * Get shortContent
     *
     * @return string $shortContent
     */
    public function getShortContent()
    {
        return $this->shortContent;
    }

    /**
     * Set fullContent
     *
     * @param string $fullContent
     * @return self
     */
    public function setFullContent($fullContent)
    {
        $this->fullContent = $fullContent;
        return $this;
    }

    /**
     * Get fullContent
     *
     * @return string $fullContent
     */
    public function getFullContent()
    {
        return $this->fullContent;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add comment
     *
     * @param Comment $comment
     */
    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;
    }

    /**
     * Remove comment
     *
     * @param Comment $comment
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return Comment[] $comments
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add file
     *
     * @param FileStorage $file
     */
    public function addFile(FileStorage $file)
    {
        $this->files[] = $file;
    }

    /**
     * Remove file
     *
     * @param FileStorage $file
     */
    public function removeFile(FileStorage $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return FileStorage[] $files
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return self
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean $enabled
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
