<?php
namespace JMD\MC\NewsBundle\Bus\Handler;

use Doctrine\ODM\MongoDB\DocumentManager;
use JMD\MC\NewsBundle\Bus\Command\UpdateCommentCommand;

class UpdateCommentCommandHandler
{
    protected $odm;

    /**
     * Dependency Injection constructor.
     *
     * @param DocumentManager $odm
     */
    public function __construct(DocumentManager $odm)
    {
        $this->odm = $odm;
    }

    public function handle(UpdateCommentCommand $command)
    {
        $comment = $this->odm
            ->getRepository('JMDMCNewsBundle:Comment')
            ->find($command->id);

        $comment
            ->setContent($command->content)
        ;

        $this->odm->persist($comment);
    }
}