<?php
namespace JMD\MC\NewsBundle\Bus\Command;

use JMD\MC\CoreBundle\Bus\Model\MessageTrait;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateCommentCommand
{
    use MessageTrait;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public $content;
}