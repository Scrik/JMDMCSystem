<?php
namespace JMD\MC\CoreBundle\Doctrine;

use Gedmo\References\ReferencesListener as BaseReferencesListener;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ReferenceListener
 * @package JMD\MC\CoreBundle\Doctrine
 */
class ReferenceListener extends BaseReferencesListener
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;
    /**
     * @var array
     */
    protected $managers
        = [
            'document' => 'doctrine.odm.mongodb.document_manager',
            'entity' => 'doctrine.orm.default_entity_manager'
        ];

    /**
     * @param ContainerInterface $container
     * @param array $managers
     */
    public function __construct(ContainerInterface $container, array $managers = array())
    {
        $this->container = $container;
        parent::__construct($managers);
    }

    /**
     * @param $type
     *
     * @return object
     */
    public function getManager($type)
    {
        return $this->container->get($this->managers[$type]);
    }
}