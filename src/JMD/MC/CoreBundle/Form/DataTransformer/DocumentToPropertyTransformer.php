<?php

namespace JMD\MC\CoreBundle\Form\DataTransformer;

use Doctrine\DBAL\Exception\DriverException;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Data transformer for single mode (i.e., multiple = false)
 *
 * Class EntityToPropertyTransformer
 * @package Tetranz\Select2EntityBundle\Form\DataTransformer
 */
class DocumentToPropertyTransformer implements DataTransformerInterface
{
    /** @var DocumentManager */
    protected $dm;
    /** @var  string */
    protected $className;
    /** @var  string */
    protected $textProperty;
    /** @var  string */
    protected $primaryKey;

    /**
     * @param DocumentManager $dm
     * @param string $class
     * @param string|null $textProperty
     * @param string $primaryKey
     */
    public function __construct(DocumentManager $dm, $class, $textProperty = null, $primaryKey = 'id')
    {
        $this->dm = $dm;
        $this->className = $class;
        $this->textProperty = $textProperty;
        $this->primaryKey = $primaryKey;
    }

    /**
     * Transform entity to array
     *
     * @param mixed $document
     * @return array
     */
    public function transform($document)
    {
        $data = array();
        if (null === $document) {
            return $data;
        }
        $accessor = PropertyAccess::createPropertyAccessor();

        $text = is_null($this->textProperty)
            ? (string)$document
            : $accessor->getValue($document, $this->textProperty);

        $data[$accessor->getValue($document, $this->primaryKey)] = $text;

        return $data;
    }

    /**
     * Transform to single id value to an entity
     *
     * @param string $value
     * @return mixed|null|object
     */
    public function reverseTransform($value)
    {
        if (null === $value) {
            return null;
        }
        $repo = $this->dm->getRepository($this->className);

        try {
          $document = $repo->find($value);
        }
        catch(DriverException $ex) {
          // this will happen if the form submits invalid data
          throw new TransformationFailedException(sprintf('The choice "%s" does not exist or is not unique', $value));
        }

        if (!$document) {
            return null;
        }

        return $document;
    }
}
