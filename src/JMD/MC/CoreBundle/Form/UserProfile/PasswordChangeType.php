<?php
namespace JMD\MC\CoreBundle\Form\UserProfile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType as PasswordFormType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PasswordType
 * @package JMD\MC\CoreBundle\Form
 */
class PasswordChangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, [
                'type' => PasswordFormType::class,
                'first_options' => [ 'label' => 'user.form.label.password' ],
                'second_options' => [ 'label' => 'user.form.label.repeat_password' ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'JMD\MC\CoreBundle\Document\User',
            'translation_domain' => 'profile',
        ]);
    }
}
