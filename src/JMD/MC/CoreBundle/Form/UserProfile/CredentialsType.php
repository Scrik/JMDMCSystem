<?php
namespace JMD\MC\CoreBundle\Form\UserProfile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CredentialsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'user.form.info.name'
            ])
            // Раскомментировать, если надо дать возможность пользователяю менять свой логин самостоятельно
//            ->add('username', TextType::class, [
//                'label' => 'user.form.info.username'
//            ])
            ->add('sign', TextareaType::class, [
                'label'     => 'user.form.info.sign',
                'required'  => false,
                'attr'      => [
                    'rows'  => 10
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'            => '\JMD\MC\CoreBundle\Document\User',
            'translation_domain'    => 'profile'
        ]);
    }
}
