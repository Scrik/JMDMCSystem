<?php
namespace JMD\MC\CoreBundle\Listener;

use JMD\MC\CoreBundle\Exception\ThemeException;
use JMD\MC\CoreBundle\Service\Settings;
use Liip\ThemeBundle\ActiveTheme;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ThemeListener implements EventSubscriberInterface
{
    /** @var ActiveTheme */
    private $liip;
    /** @var Settings */
    private $settings;
    /** @var string */
    private $rootDir;

    /**
     * ThemeListener constructor.
     * @param ActiveTheme $liip
     */
    public function __construct(ActiveTheme $liip, Settings $settings, $rootDir)
    {
        $this->liip = $liip;
        $this->settings = $settings;
        $this->rootDir = $rootDir;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [ [ 'onKernelRequest', -5 ] ]
        ];
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->isMasterRequest()) {


            $finder = new Finder();
            $dirs = $finder->directories()->in($this->rootDir.DIRECTORY_SEPARATOR.'Resources'.DIRECTORY_SEPARATOR.'themes');

            $themes = [];
            /** @var SplFileInfo $dir */
            foreach ($dirs as $dir) {
                if (empty($dir->getRelativePath())) {
                    if (!in_array($dir->getRelativePathname(), $themes)) {
                        $themes[] = $dir->getRelativePathname();
                    }
                }
            }
            
            if (count($themes) === 0) {
                throw new ThemeException('Themes not found!');
            }

            $theme = $this->settings->get('theme');
            if (null === $theme) {
                $theme = $themes[0];
            } else {
                $theme = $theme->getValue();
            }

            $this->liip->setThemes($themes);
            $this->liip->setName($theme);
        }
    }
}