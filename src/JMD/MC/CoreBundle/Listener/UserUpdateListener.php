<?php
namespace JMD\MC\CoreBundle\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Event\OnFlushEventArgs;
use Doctrine\ODM\MongoDB\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\User\UserInterface;

class UserUpdateListener
{
    /** @var UserPasswordEncoder */
    private $encoder;

    /**
     * UserEntityListener constructor.
     * @param UserPasswordEncoder $encoder
     */
    public function __construct(UserPasswordEncoder $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $document = $args->getDocument();

        if (!$document instanceof UserInterface) {
            return;
        }

        $document->setSalt(uniqid(mt_rand(), true));
        $password = $this->encoder->encodePassword($document, $document->getPassword());
        $document->setPassword($password);
    }
}