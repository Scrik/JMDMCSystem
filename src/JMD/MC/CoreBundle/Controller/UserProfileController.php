<?php
namespace JMD\MC\CoreBundle\Controller;

use JMD\MC\CoreBundle\Exception\FileStorageException;
use JMD\MC\CoreBundle\Form\UserProfile\CredentialsType;
use JMD\MC\CoreBundle\Form\UserProfile\SettingsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserProfileController
 * @package JMD\MC\CoreBundle\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 */
class UserProfileController extends BaseController
{
    public function profileAction()
    {
        return $this->render('overview.html.twig', [
            'current'   => 'overview',
            'title'     => $this->trans('title.profile.profile')
        ]);
    }

    public function avatarAction(Request $request)
    {
        $form = $this->createForm('JMD\MC\CoreBundle\Form\UserProfile\AvatarType');
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isSubmitted()) {
                if (!$form->isValid()) {
                    $errors = $form->getErrors();
                    foreach ($errors as $error) {
                        $this->getFlashBag()->add('danger', $error);
                    }
                }

                $data = $form->getData();

                try {
                    $user = $this->getUser();

                    $currentAvatar = $user->getAvatar();
                    if ($currentAvatar !== null) {
                        $this->get('jmd.mc_core.file_storage')->removeFile($currentAvatar);
                        $user->setAvatar(null);

                        $this->getODM()->persist($user);
                    }

                    if (isset($data['delete']) && $data['delete'] == true) {
                        $this->getODM()->flush();

                        $this->getFlashBag()->add('info', $this->trans('user.profile.avatar.deleted', [], 'profile'));

                        return $this->redirect($this->generateUrl('user_avatar', []));
                    }

                    if (null !== $data['avatar']) {
                        $fileHelper = $this->get('jmd.mc_core.file_storage');
                        $fileHelper->setAllowedMimeTypes([
                            'image/jpeg',
                            'image/png'
                        ])->setMaxSize(153600);

                        $imageSize = getimagesize($data['avatar']->getPathName());
                        if (!$imageSize) {
                            $this->getFlashBag()->add('danger',
                                $this->trans(
                                    'user.form.error.avatar.not_image',
                                    [ '%allowed_types%' => implode(', ', $fileHelper->getAllowedMimeTypes())],
                                    'profile'));
                            return $this->redirect($this->generateUrl('user_avatar', []));
                        }

                        $avatarSize = $this->get('jmd.mc_core.settings')->get('avatar_size');

                        if ($avatarSize !== null) {
                            list ($width, $height) = explode('x', $avatarSize->getValue());
                            if ($width < $imageSize[0] || $height < $imageSize[1]) {
                                $this->getFlashBag()->add('danger',
                                    $this->trans(
                                        'user.form.error.avatar.wrong_dimension',
                                        [ '%max_dimension%' => $avatarSize->getValue() ],
                                        'profile'));
                                return $this->redirect($this->generateUrl('user_avatar', []));
                            }
                        }

                        $fileStorage = $fileHelper->uploadFile($data['avatar'], $this->getUser());
                        $user->setAvatar($fileStorage);

                        $this->getODM()->persist($user);
                        $this->getODM()->flush();

                        $this->getFlashBag()->add('info', $this->trans('user.profile.avatar.uploaded', [], 'profile'));

                        return $this->redirect($this->generateUrl('user_avatar', []));
                    }
                } catch (Exception $e) {
                    $this->getFlashBag()->add('danger', $e->getMessage());
                    return $this->redirect($this->generateUrl('user_avatar', []));
                } catch (FileStorageException $e) {
                    $this->getFlashBag()->add('danger', $e->getMessage());
                    return $this->redirect($this->generateUrl('user_avatar', []));
                }
            }
        }

        return $this->render('avatar.html.twig', [
            'current'   => 'avatar',
            'form'      => $form->createView(),
            'title'     => $this->trans('title.profile.avatar')
        ]);
    }

    public function passwordAction(Request $request)
    {
        $form = $this->createForm('JMD\MC\CoreBundle\Form\UserProfile\PasswordChangeType');

        if ($request->getMethod() === 'POST') {
            $form->setData($this->getUser());

            $form->handleRequest($request);
            if (!$form->isValid()) {
                $errors = $form->getErrors();
                foreach ($errors as $error) {
                    $this->getFlashBag()->add('danger', $error->getMessage());
                }

                return $this->render('password.html.twig', [
                    'current' => 'password',
                    'form' => $form->createView(),
                    'title' => $this->trans('title.profile.password'),
                ]);
            }

            $user = $form->getData();
            $user->setSalt(uniqid(mt_rand(), true));
            $user->setPassword($this->get('security.password_encoder')->encodePassword($user,
                $user->getPassword()));

            $this->getFlashBag()->add('success', $this->trans('user.form.success.password', [], 'profile'));

            $this->getODM()->persist($user);
            $this->getODM()->flush();

            return $this->redirectToRoute('user_password');
        }

        return $this->render('password.html.twig', [
            'current'   => 'password',
            'form'      => $form->createView(),
            'title'     => $this->trans('title.profile.password')
        ]);
    }

    public function profileSettingsAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(CredentialsType::class, $user);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->getODM()->persist($user);
                $this->getODM()->flush();

                $this->addFlash('success', $this->trans('user.form.success.info', [], 'profile'));

                return $this->redirectToRoute('user_profile_info');
            }

            foreach ($form->getErrors() as $error) {
                $this->addFlash('danger', $error->getMessage());
            }
        }

        return $this->render('profile_settings.html.twig', [
            'current'   => 'info',
            'title'     => $this->trans('title.profile.info'),
            'form'      => $form->createView(),
        ]);
    }

    public function settingsAction(Request $request)
    {
        $form = $this->createForm(SettingsType::class);

        /**
         * Устанавливаем значения в зависимости от настроек
         */
        $data = [];
        foreach ($form->all() as $key => $child) {
            $setting = $this->get('jmd.mc_core.settings')->get($key, $this->getUser());
            $value = $setting === null ? null : (bool) $setting->getValue();
            $data[$key] = $value;
        }

        $form->setData($data);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                foreach ($form->getData() as $key => $data) {
                    $exists = $this->get('jmd.mc_core.settings')->get($key, $this->getUser());

                    if ($exists === null) {
                        $this->get('jmd.mc_core.settings')->add($key, $data, $this->getUser());
                        continue;
                    }

                    $this->get('jmd.mc_core.settings')->update($key, $data, $this->getUser());
                }

                $this->addFlash('success', $this->trans('user.form.success.settings', [], 'profile'));

                return $this->redirectToRoute('user_common_settings');
            }

            foreach ($form->getErrors() as $error) {
                $this->addFlash('danger', $error->getMessage());
            }
        }

        return $this->render('settings.html.twig', [
            'current'   => 'common',
            'title'     => $this->trans('title.profile.common'),
            'form'      => $form->createView(),
        ]);
    }
}