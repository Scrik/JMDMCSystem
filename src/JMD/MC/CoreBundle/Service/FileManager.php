<?php
namespace JMD\MC\CoreBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use JMD\MC\CoreBundle\Document\FileStorage;
use JMD\MC\CoreBundle\Namer\Transliterator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    /** @var  Transliterator */
    private $namer;

    /** @var DocumentManager */
    private $odm;

    /** @var FileStorage */
    private $fileStorage;

    /**
     * FileManager constructor.
     * @param Transliterator $namer
     */
    public function __construct(Transliterator $namer, DocumentManager $odm)
    {
        $this->namer = $namer;
        $this->odm = $odm;
    }

    /**
     * @param UploadedFile $file
     * @param string|null $uploadedName
     */
    public function upload(UploadedFile &$file, $uploadedName = null)
    {
        $fileStorage = $this->getFileStorage();
        $originalExtension = '.' . $file->getClientOriginalExtension();

        $name = str_replace($originalExtension, '', $file->getClientOriginalName());
        $uploadPath = $fileStorage->getUploadPath();

        $uploadedServerName = $uploadedName . $originalExtension;

        if (null === $uploadedName) {
            $uploadedServerName = $this->namer->generate($name, $originalExtension);
        }

        $path = $uploadPath . $uploadedServerName;

        $fileStorage
            ->setOriginalName($file->getClientOriginalName())
            ->setPath($path)
            ->setName($uploadedServerName)
            ->setSize($file->getSize())
            ->setMimeType($file->getMimeType())
        ;
        
        $file->move(
            $uploadPath,
            $uploadedServerName
        );

        $this->odm->persist($fileStorage);
    }

    /**
     * @param FileStorage $fileStorage
     * @return FileManager
     */
    public function setFileStorage(FileStorage $fileStorage)
    {
        $this->fileStorage = $fileStorage;

        return $this;
    }

    /**
     * @return FileStorage
     */
    public function getFileStorage()
    {
        if (null === $this->fileStorage) {
            $this->fileStorage = new FileStorage();
        }

        return $this->fileStorage;
    }
}