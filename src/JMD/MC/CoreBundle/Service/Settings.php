<?php
namespace JMD\MC\CoreBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use JMD\MC\CoreBundle\Document\Settings as SettingsDocument;
use JMD\MC\CoreBundle\Exception\SettingsException;

class Settings
{
    /** @var DocumentManager */
    private $odm;

    /**
     * Settings constructor.
     * @param DocumentManager $odm
     */
    public function __construct(DocumentManager $odm)
    {
        $this->odm = $odm;
    }

    /**
     * Add new setting to database
     * @param string $name
     * @param string $value
     * @param object|null $document
     * @throws SettingsException
     */
    public function add($name, $value, $document = null)
    {
        if ($this->hasSettingDublicate($name, $document)) {
            throw new SettingsException(
                'Setting "%name%" for document "%document%" has dublicate. You can\'t add same setting!',
                [
                    'name' => $name,
                    'document' => $this->getDocumentMetadata($document),
                ]
            );
        }

        $setting = new SettingsDocument();

        $setting->setName($name)
            ->setValue($value)
            ->setRefId($document);

        $this->odm->persist($setting);
        $this->odm->flush();
    }

    /**
     * Get setting by name and optional document
     *
     * @param string $name
     * @param object|null $document
     * @return SettingsDocument|null
     * @throws SettingsException
     */
    public function get($name, $document = null)
    {
        if (null !== $document && !is_object($document)) {
            throw new SettingsException(
                'Document value "%document%" is not object or null!',
                [ 'document' => $document ]
            );
        }

        $setting = $this->odm->getRepository('JMDMCCoreBundle:Settings')
            ->createQueryBuilder()
            ->field('name')->equals($name)
        ;

        if (null !== $document) {
            $setting->field('refId')->references($document);
        } else {
            $setting->field('refId')->equals($document);
        }

        return $setting->getQuery()->getSingleResult();
    }

    /**
     * Remove setting
     * @param string $name
     * @param object|null $document
     * @throws SettingsException
     */
    public function remove($name, $document = null)
    {
        if (!$this->hasSettingDublicate($name, $document)) {
            throw new SettingsException(
                'Setting "%name%" for document "%document%" does not exists. Nothing to remove!',
                [
                    'name' => $name,
                    'document' => $this->getDocumentMetadata($document),
                ]
            );
        }

        $setting = $this->get($name, $document);

        $this->odm->remove($setting);
        $this->odm->flush();
    }

    /**
     * Update setting by name and optional document with value
     * @param string $name
     * @param string $value
     * @param object|null $document
     * @throws SettingsException
     */
    public function update($name, $value, $document = null)
    {
        if (!$this->hasSettingDublicate($name, $document)) {
            throw new SettingsException(
                'Setting name "%name%" for document "%document%" does not exists! Add it first!',
                [
                    'name' => $name,
                    'document' => $this->getDocumentMetadata($document)
                ]
            );
        }

        $setting = $this->get($name, $document);
        $setting->setValue($value);

        $this->odm->persist($setting);
        $this->odm->flush();
    }

    /**
     * @param string $name
     * @param object|null $document
     * @return bool
     * @throws SettingsException
     */
    private function hasSettingDublicate($name, $document)
    {
        return $this->get($name, $document) !== null;
    }

    /**
     * Get info string for document
     *
     * @param object|null $document
     * @return string
     */
    private function getDocumentMetadata($document) {
        if (!is_object($document)) {
            return 'global';
        }

        $metadataClass = new \ReflectionClass($document);

        return sprintf(
            '%s#%s',
            $metadataClass->getName(),
            $document->getId()
        );
    }
}