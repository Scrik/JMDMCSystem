<?php
namespace JMD\MC\CoreBundle\Composer;

use Sensio\Bundle\DistributionBundle\Composer\ScriptHandler as BaseHandler;
use Composer\Script\Event;

class BBCodeHandler extends BaseHandler
{
    public static function installEmoticons(Event $event)
    {
        $options = self::getOptions($event);
        // use Symfony 3.0 dir structure if available
        $consoleDir = isset($options['symfony-bin-dir']) ? $options['symfony-bin-dir'] : $options['symfony-app-dir'];
        $event->getIO()->write('<info>Dumping emoticons...</info>');
        static::executeCommand($event, $consoleDir, 'bbcode:dump');
    }
}