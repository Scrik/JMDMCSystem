<?php
namespace JMD\MC\CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableDocument;

/**
 * Class FileStorage
 * @package JMD\MC\CoreBundle\Document
 *
 * @ODM\Document()
 */
class FileStorage
{
    use TimestampableDocument;

    /**
     * @var string
     * @ODM\Id()
     */
    private $id;

    /**
     * @var object
     * @ODM\ReferenceOne(nullable=true)
     */
    private $document;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $path;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $name;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $originalName;

    /**
     * @var float
     * @ODM\Field(type="float")
     */
    private $size;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $mimeType;

    /** @var string */
    private $uploadPath = 'upload/';

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set document
     *
     * @param $document
     * @return self
     */
    public function setDocument($document = null)
    {
        $this->document = $document;
        return $this;
    }

    /**
     * Get document
     *
     * @return $document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get path
     *
     * @return string $path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set originalName
     *
     * @param string $originalName
     * @return self
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName === null ? $this->getName() : $originalName;
        return $this;
    }

    /**
     * Get originalName
     *
     * @return string $originalName
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * Set size
     *
     * @param float $size
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Get size
     *
     * @return float $size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return self
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string $mimeType
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param $path
     *
     * @return self
     */
    public function setUploadPath($path)
    {
        $this->uploadPath = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return $this->uploadPath;
    }
}
