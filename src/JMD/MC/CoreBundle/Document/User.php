<?php
namespace JMD\MC\CoreBundle\Document;

use Doctrine\Bundle\MongoDBBundle\Validator\Constraints;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ORM\Mapping\Id;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use JMD\MC\CoreBundle\Entity\UserProxy;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package JMD\MC\CoreBundle\Document
 * @ODM\Document(repositoryClass="JMD\MC\CoreBundle\Document\Repository\User")
 * @Constraints\Unique(fields={"uuid", "name", "username"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ODM\HasLifecycleCallbacks()
 */
class User implements AdvancedUserInterface, \Serializable
{
    use SoftDeleteableDocument;
    use TimestampableDocument;

    /**
     * @var  Id
     * @ODM\Id()
     */
    private $id;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $uuid;

    /**
     * @var string
     * @ODM\Field(type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     * @ODM\Field(type="string")
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $email;

    /**
     * @var string
     * @ODM\Field(type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var string
     * @ODM\Field(type="string", nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $password;

    /**
     * @var float
     * @ODM\Field(type="float")
     */
    private $realAmount = 0;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $role = 'ROLE_USER';

    /**
     * @var string
     * @ODM\Field(type="string", nullable=false)
     */
    private $salt;

    /**
     * @var bool
     * @ODM\Field(type="bool")
     */
    private $enabled = false;

    /**
     * @var \DateTime
     * @ODM\Date()
     */
    private $expireDate;

    /**
     * @var \DateTime
     * @ODM\Date()
     */
    private $lastPasswordUpdateDate;

    /**
     * @var bool
     * @ODM\Field(type="bool")
     */
    private $locked = false;

    /**
     * @var User
     * @ODM\ReferenceMany(targetDocument="User")
     */
    private $blackList;

    /**
     * @var UserProxy
     * @Gedmo\ReferenceOne(type="entity", class="JMD\MC\CoreBundle\Entity\UserProxy", inversedBy="user", identifier="mysqlId")
     */
    private $userProxy;

    /**
     * @var int
     * @ODM\Field(type="int")
     */
    private $mysqlId;

    /**
     * @var string
     * @ODM\Field(nullable=true)
     */
    private $confirmationCode;

    /**
     * @var FileStorage
     * @ODM\ReferenceOne(targetDocument="FileStorage", nullable=true)
     */
    private $avatar;

    /**
     * @var string
     * @ODM\Field(type="string")
     * @Assert\Length(max="500")
     */
    private $sign;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Get username
     *
     * @return string $username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string $password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set realAmount
     *
     * @param float $realAmount
     * @return self
     */
    public function setRealAmount($realAmount)
    {
        $this->realAmount = $realAmount;
        return $this;
    }

    /**
     * Get realAmount
     *
     * @return float $realAmount
     */
    public function getRealAmount()
    {
        return $this->realAmount;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return self
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * Get role
     *
     * @return string $role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return self
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * Get salt
     *
     * @return string $salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set enabled
     *
     * @param bool $enabled
     * @return self
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool $enabled
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     * @return self
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime $expireDate
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * Set lastPasswordUpdateDate
     *
     * @param \DateTime $lastPasswordUpdateDate
     * @return self
     */
    public function setLastPasswordUpdateDate($lastPasswordUpdateDate)
    {
        $this->lastPasswordUpdateDate = $lastPasswordUpdateDate;
        return $this;
    }

    /**
     * Get lastPasswordUpdateDate
     *
     * @return \DateTime $lastPasswordUpdateDate
     */
    public function getLastPasswordUpdateDate()
    {
        return $this->lastPasswordUpdateDate;
    }

    /**
     * Set locked
     *
     * @param bool $locked
     * @return self
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
        return $this;
    }

    /**
     * Get locked
     *
     * @return bool $locked
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        return $this->getExpireDate() > new \DateTime('NOW');
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return !$this->getLocked();
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        $lastUpdate = $this->getLastPasswordUpdateDate();
        $willExpire = clone $lastUpdate;
        $willExpire->add(\DateInterval::createFromDateString('1 month'));

        return $lastUpdate < $willExpire;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return [ $this->getRole() ];
    }

    /**
     *
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->name,
            $this->username,
            $this->enabled,
            $this->locked
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list ($this->id, $this->name, $this->username, $this->enabled,
            $this->locked) = unserialize($serialized);
    }

    /**
     * @ODM\PrePersist()
     */
    public function prePersist()
    {
        $this->expireDate = new \DateTime('NOW');
        $this->expireDate->add(\DateInterval::createFromDateString('3 months'));
        $this->lastPasswordUpdateDate = new \DateTime();
    }


    /**
     * @return UserProxy
     */
    public function getUserProxy()
    {
        return $this->userProxy;
    }

    /**
     * @param UserProxy $userProxy
     * @return self
     */
    public function setUserProxy(UserProxy $userProxy)
    {
        $this->userProxy = $userProxy;

        return $this;
    }

    /**
     * @return int
     */
    public function getMysqlId()
    {
        return $this->mysqlId;
    }

    /**
     * @param int $mysqlId
     * @return self
     */
    public function setMysqlId($mysqlId)
    {
        $this->mysqlId = $mysqlId;

        return $this;
    }


    /**
     * Set confirmationCode
     *
     * @param string $confirmationCode
     * @return self
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
        return $this;
    }

    /**
     * Get confirmationCode
     *
     * @return string $confirmationCode
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * Set avatar
     *
     * @param \JMD\MC\CoreBundle\Document\FileStorage $avatar
     * @return self
     */
    public function setAvatar($avatar = null)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * Get avatar
     *
     * @return \JMD\MC\CoreBundle\Document\FileStorage $avatar
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return self
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * Get uuid
     *
     * @return string $uuid
     */
    public function getUuid()
    {
        return $this->uuid;
    }
    public function __construct()
    {
        $this->blackList = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add blackList
     *
     * @param User $blackList
     */
    public function addBlackList(User $blackList)
    {
        $this->blackList[] = $blackList;
    }

    /**
     * Remove blackList
     *
     * @param \JMD\MC\CoreBundle\Document\User $blackList
     */
    public function removeBlackList(User $blackList)
    {
        $this->blackList->removeElement($blackList);
    }

    /**
     * Get blackList
     *
     * @return \Doctrine\Common\Collections\Collection $blackList
     */
    public function getBlackList()
    {
        return $this->blackList;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isInBlackList(User $user)
    {
        $blackList = $this->getBlackList() === null ? [] : $this->getBlackList()->toArray();
        return in_array($user, $blackList);
    }

    /**
     * @return string
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * @param string $sign
     * @return self
     */
    public function setSign($sign)
    {
        $this->sign = $sign;

        return $this;
    }
}
