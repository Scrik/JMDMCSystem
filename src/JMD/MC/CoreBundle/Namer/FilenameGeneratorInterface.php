<?php
namespace JMD\MC\CoreBundle\Namer;

interface FilenameGeneratorInterface
{
    /**
     * Generates a new filename
     *
     * @param string - Filename without extension
     * @param string - Extension with dot: .jpg, .gif, etc
     * @param $object
     *
     * @return string
     */
    public function generate($filename, $extension, $object = null);
}
