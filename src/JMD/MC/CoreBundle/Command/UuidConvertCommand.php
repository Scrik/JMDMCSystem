<?php

namespace JMD\MC\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UuidConvertCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('jmd:uuid:convert')
            ->addArgument('username', InputArgument::REQUIRED, 'Username in string format')
            ->setDescription('Convert username to uuid');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $uuid = $this->getContainer()->get('jmd.mc_core.uuid');
        $output->writeln(sprintf('UUID for "%s": %s', $input->getArgument('username'),
            $uuid->offlineUserConvert($input->getArgument('username'))));
    }
}
