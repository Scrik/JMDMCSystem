<?php
namespace JMD\MC\CoreBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        /** @var ContainerInterface $container */
        $container = $this->container;

        $menu = $factory->createItem('...');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('News', [
            'route' => 'jmdmc_core_homepage',
        ])->setAttribute('icon', 'fa fa-home');

        $menu->addChild('Forum', [
            'route' => 'jmdmc_forum_user_category_index',
            'routeParameters' => ['_locale' => $container->get('request_stack')->getCurrentRequest()->getLocale()],
        ]);

        return $menu;
    }

    public function loggedInMenu(FactoryInterface $factory, array $options)
    {
        /** @var ContainerInterface $container */
        $container = $this->container;
        $currentUser = $container->get('security.token_storage')->getToken()->getUser();
        $odm = $container->get('doctrine.odm.mongodb.document_manager');

        $menu = $factory->createItem('...');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        $nqb = $odm->getRepository('JMDMCCoreBundle:Notification')->createQueryBuilder();
        $notifications = $nqb->field('user')->references($currentUser)->getQuery()->execute();

        if (count($notifications) > 0) {
            $notificationMenu = $menu->addChild('')
                ->setAttribute('icon', 'fa fa-bell')
                ->setAttribute('dropdown', true)
                ->setExtra('notifications', count($notifications));

            $previousDestination = null;
            $count = 1;
            $lastRootId = null;
            foreach ($notifications as $notification) {
                if ($notification->getDestination() !== $previousDestination) {
                    $notificationMenu->addChild($notification->getId(), [
                        'label' => $notification->getMessage(),
                        'route' => $notification->getRouteName(),
                        'routeParameters' => ['id' => $notification->getDestination()->getId()]
                    ])
                        ->setAttribute('icon', 'fa fa-star')
                        ->setExtra('notifications', 1);
                    $previousDestination = $notification->getDestination();
                    $lastRootId = $notification->getId();
                    $count = 1;
                } else {
                    ++$count;
                }
                $notificationMenu->getChild($lastRootId)->setExtra('notifications', $count);
            }
        }

        if ($container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $admin = $menu->addChild('Administration')->setAttributes([
                'dropdown' => true,
                'caret' => true,
                'icon'  => 'fa fa-cog',
            ]);

            $admin->addChild('Forum')
                ->setAttribute('dropdown_header', true)
                ->setAttribute('divider_append', true);

            $admin->addChild('Forums', [
                'route'           => 'jmdmc_forum_admin_forum_list',
                'routeParameters' => ['_locale' => $container->get('request_stack')->getCurrentRequest()->getLocale()],
            ]);
            $admin->addChild('Category', [
                'route'           => 'jmdmc_forum_admin_category_list',
                'routeParameters' => ['_locale' => $container->get('request_stack')->getCurrentRequest()->getLocale()],
            ]);
            $admin->addChild('Board', [
                'route'           => 'jmdmc_forum_admin_board_list',
                'routeParameters' => ['_locale' => $container->get('request_stack')->getCurrentRequest()->getLocale()],
            ]);
        }

        $user = $menu->addChild($currentUser->getUsername())->setAttributes([
            'dropdown' => true,
            'caret' => true,
            'icon' => 'fa fa-user',
        ]);

        $user->addChild('Settings')
            ->setAttribute('dropdown_header', true)
            ->setAttribute('divider_append', true)
            ->setAttribute('icon', 'fa fa-cogs')
        ;

        $user->addChild('Profile', [ 'route'   => 'user_profile' ])
            ->setAttribute('icon', 'fa fa-info');

        $user->addChild('RealAmount')
            ->setAttribute('divider_prepend', true)
            ->setAttribute('class', 'dropdown-header');
        $user->addChild($currentUser->getRealAmount())
            ->setAttribute('divider_append', true)
            ->setAttribute('class', 'dropdown-header')
            ->setAttribute('icon', 'fa fa-rub');

        $user->addChild('Cash-in', [ 'uri'   => '#' ])
            ->setAttribute('icon', 'fa fa-money');
        $user->addChild('Basket', [ 'uri'   => '#' ])
            ->setAttribute('divider_append', true)
            ->setAttribute('icon', 'fa fa-shopping-cart');

        $user->addChild('Suggest news', [
                'route' => 'jmdmc_news_create',
            ])
            ->setAttribute('icon', 'fa fa-pencil-square');
        $user->addChild('My news', [ 'route' => 'jmdmc_news_user_list' ])
            ->setAttribute('icon', 'fa fa-list');

        $user->addChild('Logout', [ 'route' => 'logout' ])
            ->setAttribute('icon', 'fa fa-sign-out')
            ->setAttribute('divider_prepend', true);

        return $menu;
    }

    /**
     * Login/register right menu
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function signInUpMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('...');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        $menu->addChild('Login', [
            'uri'   => '#',
        ])->setAttributes([
            'data-toggle' => 'modal',
            'data-target' => '#login',
            'icon'        => 'fa fa-sign-in'
        ]);

        $menu->addChild('Register', [
            'uri'   => '#',
        ])->setAttributes([
            'data-toggle' => 'modal',
            'data-target' => '#register',
            'icon'  => 'fa fa-key',
        ]);

        return $menu;
    }
}