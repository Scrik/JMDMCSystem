<?php
namespace JMD\MC\ForumBundle\Model\Component\Repository;

use JMD\MC\ForumBundle\Model\Component\Repository\Repository;
use JMD\MC\ForumBundle\Model\Component\Repository\RepositoryInterface;

/**
 * ForumRepository
 *
 * @category matuck
 * @package  ForumBundle
 *
 * @author   Mitch Tuck <matuck@matuck.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @version  Release: 2.0
 * @link     https://github.com/matuck/JMDMCForumBundle
 */
class ForumRepository extends BaseRepository implements RepositoryInterface
{
    /**
     *
     * @access public
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllForums()
    {
        $qb = $this->createSelectQuery(array('f'));

        return $this->gateway->findForums($qb);
    }

    /**
     *
     * @access public
     * @param  int                                 $forumId
     * @return \JMD\MC\ForumBundle\Entity\Forum
     */
    public function findOneForumById($forumId)
    {
        $params = array(':forumId' => $forumId);

        $qb = $this->createSelectQuery(array('f'));

        $qb
            ->where('f.id = :forumId');
        ;

        return $this->gateway->findForum($qb, $params);
    }

    /**
     *
     * @access public
     * @param  string                              $forumName
     * @return \JMD\MC\ForumBundle\Entity\Forum
     */
    public function findOneForumByName($forumName)
    {
        $params = array(':forumName' => $forumName);

        $qb = $this->createSelectQuery(array('f'));

        $qb
            ->where('f.name = :forumName');
        ;

        return $this->gateway->findForum($qb, $params);
    }
}
