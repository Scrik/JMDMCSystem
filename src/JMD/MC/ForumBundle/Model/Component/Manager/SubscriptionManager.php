<?php
namespace JMD\MC\ForumBundle\Model\Component\Manager;

use Symfony\Component\Security\Core\User\UserInterface;

use JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface;
use JMD\MC\ForumBundle\Model\Component\Manager\BaseManager;

use JMD\MC\ForumBundle\Entity\Subscription;
use JMD\MC\ForumBundle\Entity\Topic;

class SubscriptionManager extends BaseManager implements ManagerInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Subscription
     */
    public function createSubscription()
    {
        return $this->gateway->createSubscription();
    }

    /**
     *
     * @access public
     * @param  int                                                 $topicId
     * @param  \Symfony\Component\Security\Core\User\UserInterface $userId
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function subscribe(Topic $topic, $user)
    {
        $subscription = $this->model->findOneSubscriptionForTopicByIdAndUserById($topic->getId(), $user->getId());

        if (! $subscription) {
            $subscription = new Subscription();
        }

        if (! $subscription->isSubscribed()) {
            $subscription->setSubscribed(true);
            $subscription->setOwnedBy($user);
            $subscription->setTopic($topic);
            $subscription->setRead(true);
            $subscription->setForum($topic->getBoard()->getCategory()->getForum());

            $this->gateway->saveSubscription($subscription);
        }

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic             $topic
     * @param  int                                             $userId
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function unsubscribe(Topic $topic, $userId)
    {
        $subscription = $this->model->findOneSubscriptionForTopicByIdAndUserById($topic->getId(), $userId);

        if (! $subscription) {
            return $this;
        }

        $subscription->setSubscribed(false);
        $subscription->setRead(true);

        $this->gateway->saveSubscription($subscription);

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Subscription      $subscription
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function markAsRead(Subscription $subscription)
    {
        $subscription->setRead(true);

        $this->persist($subscription)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Subscription      $subscription
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function markAsUnread(Subscription $subscription)
    {
        $subscription->setRead(false);

        $this->persist($subscription)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \Doctrine\Common\Collections\ArrayCollection        $subscriptions
     * @param  \Symfony\Component\Security\Core\User\UserInterface $exceptUser
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function markTheseAsUnread($subscriptions, $exceptUser)
    {
        foreach ($subscriptions as $subscription) {
            if ($subscription->getOwnedBy()) {
                if ($subscription->getOwnedBy()->getId() != $exceptUser->getId()) {
                    $subscription->setRead(false);

                    $this->persist($subscription);
                }
            }
        }

        $this->flush();
    }
}
