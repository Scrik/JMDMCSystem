<?php
namespace JMD\MC\ForumBundle\Model\Component\Manager;

use Symfony\Component\Security\Core\User\UserInterface;

use JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface;
use JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface;
use JMD\MC\ForumBundle\Model\Component\Manager\BaseManager;
use JMD\MC\ForumBundle\Component\Helper\PostLockHelper;

use JMD\MC\ForumBundle\Entity\Topic;

class TopicManager extends BaseManager implements ManagerInterface
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Component\Helper\PostLockHelper $postLockHelper
     */
    protected $postLockHelper;

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Gateway\GatewayInterface        $gateway
     * @param \JMD\MC\ForumBundle\Component\Helper\PostLockHelper $postLockHelper
     */
    public function __construct(GatewayInterface $gateway, PostLockHelper $postLockHelper)
    {
        $this->gateway = $gateway;
        $this->postLockHelper = $postLockHelper;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function createTopic()
    {
        return $this->gateway->createTopic();
    }

    /**
     *
     * Post must have a set topic for topic to be set  correctly.
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post              $post
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function saveTopic(Topic $topic)
    {
        $this->gateway->saveTopic($topic);

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic             $topic
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function updateTopic(Topic $topic)
    {
        $this->gateway->updateTopic($topic);

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic             $topic
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function incrementViewCounter(Topic $topic)
    {
        // set the new counters
        $topic->setCachedViewCount($topic->getCachedViewCount() + 1);

        $this->persist($topic)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                 $topic
     * @param  \Symfony\Component\Security\Core\User\UserInterface $user
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function softDelete(Topic $topic, $user)
    {
        // Don't overwite previous users accountability.
        if (! $topic->getDeletedBy() && ! $topic->getDeletedDate()) {
            $topic->setDeleted(true);
            $topic->setDeletedBy($user);
            $topic->setDeletedDate(new \DateTime());

            // Close the topic as a precaution.
            $topic->setClosed(true);
            $topic->setClosedBy($user);
            $topic->setClosedDate(new \DateTime());

            // update the record before doing record counts
            $this->persist($topic)->flush();
        }

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic             $topic
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function restore(Topic $topic)
    {
        $topic->setDeleted(false);
        $topic->setDeletedBy(null);
        $topic->setDeletedDate(null);

        $this->persist($topic)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic             $topic
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function sticky(Topic $topic, $user)
    {
        $topic->setSticky(true);
        $topic->setStickiedBy($user);
        $topic->setStickiedDate(new \DateTime());

        $this->persist($topic)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic             $topic
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function unsticky(Topic $topic)
    {
        $topic->setSticky(false);
        $topic->setStickiedBy(null);
        $topic->setStickiedDate(null);

        $this->persist($topic)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                 $topic
     * @param  \Symfony\Component\Security\Core\User\UserInterface $user
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function close(Topic $topic, $user)
    {
        // Don't overwite previous users accountability.
        if (! $topic->getClosedBy() && ! $topic->getClosedDate()) {
            $topic->setClosed(true);
            $topic->setClosedBy($user);
            $topic->setClosedDate(new \DateTime());

            $this->persist($topic)->flush();
        }

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic             $topic
     * @return \JMD\MC\ForumBundle\Manager\ManagerInterface
     */
    public function reopen(Topic $topic)
    {
        $topic->setClosed(false);
        $topic->setClosedBy(null);
        $topic->setClosedDate(null);

        if ($topic->isDeleted()) {
            $topic->setDeleted(false);
            $topic->setDeletedBy(null);
            $topic->setDeletedDate(null);
        }

        $this->persist($topic)->flush();

        return $this;
    }
}
