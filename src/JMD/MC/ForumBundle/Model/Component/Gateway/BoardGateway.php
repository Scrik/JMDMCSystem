<?php
namespace JMD\MC\ForumBundle\Model\Component\Gateway;

use Doctrine\ORM\QueryBuilder;
use JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface;
use JMD\MC\ForumBundle\Model\Component\Gateway\BaseGateway;
use JMD\MC\ForumBundle\Entity\Board;

class BoardGateway extends BaseGateway implements GatewayInterface
{
    /**
     *
     * @access private
     * @var string $queryAlias
     */
    protected $queryAlias = 'b';

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findBoard(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        $qb->addOrderBy('b.listOrderPriority', 'ASC');

        return $this->one($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder                   $qb
     * @param  Array                                        $parameters
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findBoards(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createSelectQuery();
        }

        $qb->addOrderBy('b.listOrderPriority', 'ASC');

        return $this->all($qb, $parameters);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\ORM\QueryBuilder $qb
     * @param  Array                      $parameters
     * @return int
     */
    public function countBoards(QueryBuilder $qb = null, $parameters = null)
    {
        if (null == $qb) {
            $qb = $this->createCountQuery();
        }

        if (null == $parameters) {
            $parameters = array();
        }

        $qb->setParameters($parameters);

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $board
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function saveBoard(Board $board)
    {
        $this->persist($board)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $board
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function updateBoard(Board $board)
    {
        $this->persist($board)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                             $board
     * @return \JMD\MC\ForumBundle\Model\Component\Gateway\GatewayInterface
     */
    public function deleteBoard(Board $board)
    {
        $this->remove($board)->flush();

        return $this;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Board
     */
    public function createBoard()
    {
        return new $this->entityClass();
    }
}
