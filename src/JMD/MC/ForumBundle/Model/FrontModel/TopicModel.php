<?php
namespace JMD\MC\ForumBundle\Model\FrontModel;

use Symfony\Component\Security\Core\User\UserInterface;
use JMD\MC\ForumBundle\Model\FrontModel\BaseModel;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Topic;

class TopicModel extends BaseModel implements ModelInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function createTopic()
    {
        return $this->getManager()->createTopic();
    }

    /**
     *
     * @access public
     * @param  int                                                      $boardId
     * @param  int                                                      $page
     * @param  int                                                      $itemsPerPage
     * @param  bool                                                     $canViewDeletedTopics
     * @return \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination
     */
    public function findAllTopicsPaginatedByBoardId($boardId, $page, $itemsPerPage = 25, $canViewDeletedTopics = false)
    {
        return $this->getRepository()->findAllTopicsPaginatedByBoardId($boardId, $page, $itemsPerPage, $canViewDeletedTopics);
    }

    /**
     *
     * @access public
     * @param  int                                                      $boardId
     * @param  bool                                                     $canViewDeletedTopics
     * @return \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination
     */
    public function findAllTopicsStickiedByBoardId($boardId, $canViewDeletedTopics = false)
    {
        return $this->getRepository()->findAllTopicsStickiedByBoardId($boardId, $canViewDeletedTopics);
    }

    /**
     *
     * @access public
     * @param  int                                 $topicId
     * @param  bool                                $canViewDeletedTopics
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function findOneTopicByIdWithBoardAndCategory($topicId, $canViewDeletedTopics = false)
    {
        return $this->getRepository()->findOneTopicByIdWithBoardAndCategory($topicId, $canViewDeletedTopics);
    }

    /**
     *
     * @access public
     * @param  int                                 $topicId
     * @param  bool                                $canViewDeletedTopics
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function findOneTopicByIdWithPosts($topicId, $canViewDeletedTopics = false)
    {
        return $this->getRepository()->findOneTopicByIdWithPosts($topicId, $canViewDeletedTopics);
    }

    /**
     *
     * @access public
     * @param  int                                 $topicId
     * @return \JMD\MC\ForumBundle\Entity\Topic
     */
    public function findLastTopicForBoardByIdWithLastPost($boardId)
    {
        return $this->getRepository()->findLastTopicForBoardByIdWithLastPost($boardId);
    }

    /**
     *
     * @access public
     * @param  int   $boardId
     * @return Array
     */
    public function getTopicAndPostCountForBoardById($boardId)
    {
        return $this->getRepository()->getTopicAndPostCountForBoardById($boardId);
    }

    /**
     *
     * Post must have a set topic for topic to be set correctly.
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function saveTopic(Topic $topic)
    {
        return $this->getManager()->saveTopic($topic);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function updateTopic(Topic $topic)
    {
        return $this->getManager()->updateTopic($topic);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function incrementViewCounter(Topic $topic)
    {
        return $this->getManager()->incrementViewCounter($topic);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function restore(Topic $topic)
    {
        return $this->getManager()->restore($topic);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @param  \Symfony\Component\Security\Core\User\UserInterface             $user
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function softDelete(Topic $topic, $user)
    {
        return $this->getManager()->softDelete($topic, $user);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @param  \Symfony\Component\Security\Core\User\UserInterface             $user
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function sticky(Topic $topic, $user)
    {
        return $this->getManager()->sticky($topic, $user);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function unsticky(Topic $topic)
    {
        return $this->getManager()->unsticky($topic);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @param  \Symfony\Component\Security\Core\User\UserInterface             $user
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function close(Topic $topic, $user)
    {
        return $this->getManager()->close($topic, $user);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Topic                             $topic
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function reopen(Topic $topic)
    {
        return $this->getManager()->reopen($topic);
    }
}
