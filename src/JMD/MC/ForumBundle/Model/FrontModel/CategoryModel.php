<?php
namespace JMD\MC\ForumBundle\Model\FrontModel;

use Doctrine\Common\Collections\ArrayCollection;
use JMD\MC\ForumBundle\Model\FrontModel\BaseModel;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Category;

class CategoryModel extends BaseModel implements ModelInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Category
     */
    public function createCategory()
    {
        return $this->getManager()->createCategory();
    }

    /**
     *
     * @access public
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllCategories()
    {
        return $this->getRepository()->findAllCategories();
    }

    /**
     *
     * @access public
     * @param  int                                          $forumId
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllCategoriesForForumById($forumId)
    {
        return $this->getRepository()->findAllCategoriesForForumById($forumId);
    }

    /**
     *
     * @access public
     * @param  string                                       $forumName
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllCategoriesWithBoardsForForumByName($forumName)
    {
        return $this->getRepository()->findAllCategoriesWithBoardsForForumByName($forumName);
    }

    /**
     *
     * @access public
     * @param  int                                    $categoryId
     * @return \JMD\MC\ForumBundle\Entity\Category
     */
    public function findOneCategoryById($categoryId)
    {
        return $this->getRepository()->findOneCategoryById($categoryId);
    }

    /**
     *
     * @access public
     * @param  int                                    $categoryId
     * @return \JMD\MC\ForumBundle\Entity\Category
     */
    public function findOneCategoryByIdWithBoards($categoryId)
    {
        return $this->getRepository()->findOneCategoryByIdWithBoards($categoryId);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Category                          $category
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function saveCategory(Category $category)
    {
        return $this->getManager()->saveCategory($category);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Category                          $category
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function updateCategory(Category $category)
    {
        return $this->getManager()->updateCategory($category);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Category                          $category
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function deleteCategory(Category $category)
    {
        return $this->getManager()->deleteCategory($category);
    }

    /**
     *
     * @access public
     * @param  \Doctrine\Common\Collections\ArrayCollection                    $boards
     * @param  \JMD\MC\ForumBundle\Entity\Category                          $category
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function reassignBoardsToCategory(ArrayCollection $boards, Category $category = null)
    {
        return $this->getManager()->reassignBoardsToCategory($boards, $category);
    }

    /**
     *
     * @access public
     * @param  Array                                                           $categories
     * @param  \JMD\MC\ForumBundle\Entity\Category                          $categoryShift
     * @param  int                                                             $direction
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function reorderCategories($categories, Category $categoryShift, $direction)
    {
        return $this->getManager()->reorderCategories($categories, $categoryShift, $direction);
    }
}
