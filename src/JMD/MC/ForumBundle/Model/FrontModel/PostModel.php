<?php
namespace JMD\MC\ForumBundle\Model\FrontModel;

use Symfony\Component\Security\Core\User\UserInterface;
use JMD\MC\ForumBundle\Model\FrontModel\BaseModel;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Post;

class PostModel extends BaseModel implements ModelInterface
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Post
     */
    public function createPost()
    {
        return $this->getManager()->createPost();
    }

    /**
     *
     * @access public
     * @param  int                                                      $topicId
     * @param  int                                                      $page
     * @param  int                                                      $itemsPerPage
     * @param  bool                                                     $canViewDeletedTopics
     * @return \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination
     */
    public function findAllPostsPaginatedByTopicId($topicId, $page, $itemsPerPage = 25, $canViewDeletedTopics = false)
    {
        return $this->getRepository()->findAllPostsPaginatedByTopicId($topicId, $page, $itemsPerPage, $canViewDeletedTopics);
    }

    /**
     *
     * @access public
     * @param  int                                $postId
     * @param  bool                               $canViewDeletedTopics
     * @return \JMD\MC\ForumBundle\Entity\Post
     */
    public function findOnePostByIdWithTopicAndBoard($postId, $canViewDeletedTopics = false)
    {
        return $this->getRepository()->findOnePostByIdWithTopicAndBoard($postId, $canViewDeletedTopics);
    }

    /**
     *
     * @access public
     * @param  int                                $topicId
     * @return \JMD\MC\ForumBundle\Entity\Post
     */
    public function getFirstPostForTopicById($topicId)
    {
        return $this->getRepository()->getFirstPostForTopicById($topicId);
    }

    /**
     *
     * @access public
     * @param  int                                $topicId
     * @return \JMD\MC\ForumBundle\Entity\Post
     */
    public function getLastPostForTopicById($topicId)
    {
        return $this->getRepository()->getLastPostForTopicById($topicId);
    }

    /**
     *
     * @access public
     * @param  int   $topicId
     * @return Array
     */
    public function countPostsForTopicById($topicId)
    {
        return $this->getRepository()->countPostsForTopicById($topicId);
    }

    /**
     *
     * @access public
     * @param  int   $topicId
     * @return Array
     */
    public function countPostsForUserById($userId)
    {
        return $this->getRepository()->countPostsForUserById($userId);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function savePost(Post $post)
    {
        return $this->getManager()->savePost($post);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function updatePost(Post $post)
    {
        return $this->getManager()->updatePost($post);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function lock(Post $post)
    {
        return $this->getManager()->lock($post);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function restore(Post $post)
    {
        return $this->getManager()->restore($post);
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                              $post
     * @param  \Symfony\Component\Security\Core\User\UserInterface             $user
     * @return \JMD\MC\ForumBundle\Model\Component\Manager\ManagerInterface
     */
    public function softDelete(Post $post, $user)
    {
        return $this->getManager()->softDelete($post, $user);
    }
}
