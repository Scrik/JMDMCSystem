<?php
namespace JMD\MC\ForumBundle\Model\FrontModel;

use Symfony\Component\Security\Core\User\UserInterface;
use JMD\MC\ForumBundle\Model\FrontModel\BaseModel;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Registry;

class RegistryModel extends BaseModel implements ModelInterface
{
    /**
     *
     * @access public
     * @param  \Symfony\Component\Security\Core\User\UserInterface $user
     * @return \JMD\MC\ForumBundle\Entity\Registry
     */
    public function findOrCreateOneRegistryForUser($user)
    {
        $registry = $this->findOneRegistryForUserById($user->getId());

        if (! $registry) {
            $registry = $this->createRegistry();
            $registry->setOwnedBy($user);
            $this->saveRegistry($registry);
        }

        return $registry;
    }

    /**
     *
     * @access public
     * @param  int                                    $userId
     * @return \JMD\MC\ForumBundle\Entity\Registry
     */
    public function findOneRegistryForUserById($userId)
    {
        return $this->getRepository()->findOneRegistryForUserById($userId);
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Registry
     */
    public function createRegistry()
    {
        return $this->getManager()->createRegistry();
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Registry                $registryModel
     * @return \JMD\MC\ForumBundle\Model\FrontModel\RegistryModel
     */
    public function saveRegistry(Registry $registry)
    {
        $this->getManager()->saveRegistry($registry);

        return $this;
    }
}
