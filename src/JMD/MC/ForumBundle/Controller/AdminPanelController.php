<?php
namespace JMD\MC\ForumBundle\Controller;

class AdminPanelController extends BaseController
{
    /**
     *
     * @access public
     * @return RenderResponse
     */
    public function indexAction()
    {
        return $this->redirectResponse($this->path('jmdmc_forum_admin_forum_list'));
    }
}
