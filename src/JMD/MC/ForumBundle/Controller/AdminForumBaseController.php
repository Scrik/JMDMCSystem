<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Entity\Forum;

class AdminForumBaseController extends BaseController
{
    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\ForumCreateFormHandler
     */
    protected function getFormHandlerToCreateForum()
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.forum_create');

        $formHandler->setRequest($this->getRequest());

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\ForumUpdateFormHandler
     */
    protected function getFormHandlerToUpdateForum(Forum $forum)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.forum_update');

        $formHandler->setRequest($this->getRequest());

        $formHandler->setForum($forum);

        return $formHandler;
    }

    /**
     *
     * @access protected
     * @return \JMD\MC\ForumBundle\Form\Handler\ForumDeleteFormHandler
     */
    protected function getFormHandlerToDeleteForum(Forum $forum)
    {
        $formHandler = $this->container->get('jmdmc_forum.form.handler.forum_delete');

        $formHandler->setRequest($this->getRequest());

        $formHandler->setForum($forum);

        return $formHandler;
    }
}
