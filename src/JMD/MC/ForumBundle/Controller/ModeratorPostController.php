<?php
namespace JMD\MC\ForumBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\ModeratorPostEvent;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\ModeratorPostResponseEvent;

class ModeratorPostController extends ModeratorPostBaseController
{
    /**
     * Lock to prevent editing of post.
     *
     * @access public
     * @param  string           $forumName
     * @param  int              $postId
     * @return RedirectResponse
     */
    public function lockAction($forumName, $postId)
    {
        $this->isAuthorised('ROLE_MODERATOR');
        $this->isFound($forum = $this->getForumModel()->findOneForumByName($forumName));
        $this->isFound($post = $this->getPostModel()->findOnePostByIdWithTopicAndBoard($postId, true));
        $this->isAuthorised($this->getAuthorizer()->canLockPost($post, $forum));
        $this->getPostModel()->lock($post);
        $this->dispatch(ForumEvents::MODERATOR_POST_LOCK_COMPLETE, new ModeratorPostEvent($this->getRequest(), $post));

        return $this->redirectResponse($this->path('jmdmc_forum_user_topic_show', array(
            'forumName' => $forumName,
            'topicId' => $post->getTopic()->getId()
        )));
    }

    /**
     *
     * @access public
     * @param  string           $forumName
     * @param  int              $postId
     * @return RedirectResponse
     */
    public function unlockAction($forumName, $postId)
    {
        $this->isAuthorised('ROLE_MODERATOR');
        $this->isFound($forum = $this->getForumModel()->findOneForumByName($forumName));
        $this->isFound($post = $this->getPostModel()->findOnePostByIdWithTopicAndBoard($postId, true));
        $this->isAuthorised($this->getAuthorizer()->canUnlockPost($post, $forum));
        $formHandler = $this->getFormHandlerToUnlockPost($post);
        $response = $this->renderResponse('JMDMCForumBundle:Moderator:Post/unlock.html.', array(
            'crumbs' => $this->getCrumbs()->addModeratorPostUnlock($forum, $post),
            'forum' => $forum,
            'forumName' => $forumName,
            'topic' => $post->getTopic(),
            'post' => $post,
            'form' => $formHandler->getForm()->createView(),
        ));
        $this->dispatch(ForumEvents::MODERATOR_POST_UNLOCK_RESPONSE, new ModeratorPostResponseEvent($this->getRequest(), $response, $formHandler->getForm()->getData()));

        return $response;
    }

    /**
     *
     * @access public
     * @param  string           $forumName
     * @param  int              $postId
     * @return RedirectResponse
     */
    public function unlockProcessAction($forumName, $postId)
    {
        $this->isAuthorised('ROLE_MODERATOR');
        $this->isFound($forum = $this->getForumModel()->findOneForumByName($forumName));
        $this->isFound($post = $this->getPostModel()->findOnePostByIdWithTopicAndBoard($postId, true));
        $this->isAuthorised($this->getAuthorizer()->canUnlockPost($post, $forum));
        $formHandler = $this->getFormHandlerToUnlockPost($post);

        if ($formHandler->process()) {
            $response = $this->redirectResponseForTopicOnPageFromPost($forumName, $post->getTopic(), $post);
        } else {
            $response = $this->renderResponse('JMDMCForumBundle:Moderator:Post/unlock.html.', array(
                'crumbs' => $this->getCrumbs()->addModeratorPostUnlock($forum, $post->getTopic()),
                'forum' => $forum,
                'forumName' => $forumName,
                'topic' => $post->getTopic(),
                'post' => $post,
                'form' => $formHandler->getForm()->createView(),
            ));
        }
        $this->dispatch(ForumEvents::MODERATOR_POST_UNLOCK_RESPONSE, new ModeratorPostResponseEvent($this->getRequest(), $response, $formHandler->getForm()->getData()));

        return $response;
    }

    /**
     *
     * @access public
     * @param  string           $forumName
     * @param  int              $postId
     * @return RedirectResponse
     */
    public function restoreAction($forumName, $postId)
    {
        $this->isAuthorised('ROLE_MODERATOR');
        $this->isFound($forum = $this->getForumModel()->findOneForumByName($forumName));
        $this->isFound($post = $this->getPostModel()->findOnePostByIdWithTopicAndBoard($postId, true));
        $this->isAuthorised($this->getAuthorizer()->canRestorePost($post, $forum));
        $this->getPostModel()->restore($post)->flush();
        $this->dispatch(ForumEvents::MODERATOR_POST_RESTORE_COMPLETE, new ModeratorPostEvent($this->getRequest(), $post));

        return $this->redirectResponse($this->path('jmdmc_forum_user_topic_show', array(
            'forumName' => $forumName,
            'topicId' => $post->getTopic()->getId()
        )));
    }
}
