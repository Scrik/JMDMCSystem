<?php
namespace JMD\MC\ForumBundle\Controller;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\AdminCategoryEvent;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\AdminCategoryResponseEvent;

class AdminCategoryController extends AdminCategoryBaseController
{
    /**
     *
     * @access public
     * @return RenderResponse
     */
    public function listAction()
    {
        $this->isAuthorised('ROLE_ADMIN');
        $forumFilter = $this->getQuery('forum_filter', null);
        $forums = $this->getForumModel()->findAllForums();
        $categories = $this->getCategoryModel()->findAllCategoriesForForumById($forumFilter);
        $response = $this->renderResponse('JMDMCForumBundle:Admin:/Category/list.html.', array(
            'crumbs' => $this->getCrumbs()->addAdminManageCategoriesIndex(),
            'forums' => $forums,
            'forum_filter' => $forumFilter,
            'categories' => $categories,
        ));

        return $response;
    }

    /**
     *
     * @access public
     * @return RenderResponse
     */
    public function createAction()
    {
        $this->isAuthorised('ROLE_ADMIN');
        $forumFilter = $this->getQuery('forum_filter', null);
        $formHandler = $this->getFormHandlerToCreateCategory($forumFilter);
        $response = $this->renderResponse('JMDMCForumBundle:Admin:/Category/create.html.', array(
            'crumbs' => $this->getCrumbs()->addAdminManageCategoriesCreate(),
            'form' => $formHandler->getForm()->createView(),
            'forum_filter' => $forumFilter
        ));

        $this->dispatch(ForumEvents::ADMIN_CATEGORY_CREATE_RESPONSE, new AdminCategoryResponseEvent($this->getRequest(), $response, null));

        return $response;
    }

    /**
     *
     * @access public
     * @return RenderResponse
     */
    public function createProcessAction()
    {
        $this->isAuthorised('ROLE_ADMIN');
        $forumFilter = $this->getQuery('forum_filter', null);
        $formHandler = $this->getFormHandlerToCreateCategory($forumFilter);

        if ($formHandler->process()) {
            $response = $this->redirectResponse($this->path('jmdmc_forum_admin_category_list', $this->getFilterQueryStrings($formHandler->getForm()->getData())));
        } else {
            $response = $this->renderResponse('JMDMCForumBundle:Admin:/Category/create.html.', array(
                'crumbs' => $this->getCrumbs()->addAdminManageCategoriesCreate(),
                'form' => $formHandler->getForm()->createView(),
                'forum_filter' => $forumFilter
            ));
        }

        $this->dispatch(ForumEvents::ADMIN_CATEGORY_CREATE_RESPONSE, new AdminCategoryResponseEvent($this->getRequest(), $response, $formHandler->getForm()->getData()));

        return $response;
    }

    /**
     *
     * @access public
     * @return RenderResponse
     */
    public function editAction($categoryId)
    {
        $this->isAuthorised('ROLE_ADMIN');
        $this->isFound($category = $this->getCategoryModel()->findOneCategoryById($categoryId));
        $formHandler = $this->getFormHandlerToUpdateCategory($category);
        $response = $this->renderResponse('JMDMCForumBundle:Admin:/Category/edit.html.', array(
            'crumbs' => $this->getCrumbs()->addAdminManageCategoriesEdit($category),
            'form' => $formHandler->getForm()->createView(),
            'category' => $category,
            'forum_filter' => $this->getQuery('forum_filter', null)
        ));

        $this->dispatch(ForumEvents::ADMIN_CATEGORY_EDIT_RESPONSE, new AdminCategoryResponseEvent($this->getRequest(), $response, $formHandler->getForm()->getData()));

        return $response;
    }

    /**
     *
     * @access public
     * @return RenderResponse
     */
    public function editProcessAction($categoryId)
    {
        $this->isAuthorised('ROLE_ADMIN');
        $this->isFound($category = $this->getCategoryModel()->findOneCategoryById($categoryId));
        $formHandler = $this->getFormHandlerToUpdateCategory($category);

        if ($formHandler->process()) {
            $response = $this->redirectResponse($this->path('jmdmc_forum_admin_category_list', $this->getFilterQueryStrings($formHandler->getForm()->getData())));
        } else {
            $response = $this->renderResponse('JMDMCForumBundle:Admin:/Category/edit.html.', array(
                'crumbs' => $this->getCrumbs()->addAdminManageCategoriesEdit($category),
                'form' => $formHandler->getForm()->createView(),
                'category' => $category,
                'forum_filter' => $this->getQuery('forum_filter', null)
            ));
        }

        $this->dispatch(ForumEvents::ADMIN_CATEGORY_EDIT_RESPONSE, new AdminCategoryResponseEvent($this->getRequest(), $response, $formHandler->getForm()->getData()));

        return $response;
    }

    /**
     *
     * @access public
     * @return RenderResponse
     */
    public function deleteAction($categoryId)
    {
        $this->isAuthorised('ROLE_SUPER_ADMIN');
        $this->isFound($category = $this->getCategoryModel()->findOneCategoryById($categoryId));
        $formHandler = $this->getFormHandlerToDeleteCategory($category);
        $response = $this->renderResponse('JMDMCForumBundle:Admin:/Category/delete.html.', array(
            'crumbs' => $this->getCrumbs()->addAdminManageCategoriesDelete($category),
            'form' => $formHandler->getForm()->createView(),
            'category' => $category,
            'forum_filter' => $this->getQuery('forum_filter', null)
        ));

        $this->dispatch(ForumEvents::ADMIN_CATEGORY_DELETE_RESPONSE, new AdminCategoryResponseEvent($this->getRequest(), $response, $formHandler->getForm()->getData()));

        return $response;
    }

    /**
     *
     * @access public
     * @return RedirectResponse
     */
    public function deleteProcessAction($categoryId)
    {
        $this->isAuthorised('ROLE_SUPER_ADMIN');
        $this->isFound($category = $this->getCategoryModel()->findOneCategoryById($categoryId));
        $formHandler = $this->getFormHandlerToDeleteCategory($category);

        if ($formHandler->process()) {
            $response = $this->redirectResponse($this->path('jmdmc_forum_admin_category_list', $this->getFilterQueryStrings($formHandler->getForm()->getData())));
        } else {
            $response = $this->renderResponse('JMDMCForumBundle:Admin:/Category/delete.html.', array(
                'crumbs' => $this->getCrumbs()->addAdminManageCategoriesDelete($category),
                'form' => $formHandler->getForm()->createView(),
                'category' => $category,
                'forum_filter' => $this->getQuery('forum_filter', null)
            ));
        }

        $this->dispatch(ForumEvents::ADMIN_CATEGORY_DELETE_RESPONSE, new AdminCategoryResponseEvent($this->getRequest(), $response, $formHandler->getForm()->getData()));

        return $response;
    }

    /**
     *
     * @access public
     * @return RedirectResponse
     */
    public function reorderAction($categoryId, $direction)
    {
        $this->isAuthorised('ROLE_ADMIN');
        $this->isFound($category = $this->getCategoryModel()->findOneCategoryById($categoryId));
        $this->dispatch(ForumEvents::ADMIN_CATEGORY_REORDER_INITIALISE, new AdminCategoryEvent($this->getRequest(), $category));
        $params = array();

        if ($category->getForum()) { // We do not re-order categories not set to a forum.
            $forumFilter = $category->getForum()->getId();
            $params['forum_filter'] = $forumFilter;
            $categories = $this->getCategoryModel()->findAllCategoriesForForumById($forumFilter);
            $this->getCategoryModel()->reorderCategories($categories, $category, $direction);
            $this->dispatch(ForumEvents::ADMIN_CATEGORY_REORDER_COMPLETE, new AdminCategoryEvent($this->getRequest(), $category));
        }

        $response = $this->redirectResponse($this->path('jmdmc_forum_admin_category_list', $params));
        $this->dispatch(ForumEvents::ADMIN_CATEGORY_REORDER_RESPONSE, new AdminCategoryResponseEvent($this->getRequest(), $response, $category));

        return $response;
    }
}
