<?php
namespace JMD\MC\ForumBundle\Component\Crumbs;

use JMD\MC\ForumBundle\Entity\Forum;
use JMD\MC\ForumBundle\Entity\Category;
use JMD\MC\ForumBundle\Entity\Board;
use JMD\MC\ForumBundle\Entity\Topic;
use JMD\MC\ForumBundle\Entity\Post;

class CrumbBuilder extends BaseCrumbBuilder
{
    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminIndex()
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
        ;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageForumsIndex()
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-forums.index', 'jmdmc_forum_admin_forum_list')
        ;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageForumsCreate()
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-forums.index', 'jmdmc_forum_admin_forum_list')
            ->add('crumbs.admin.manage-forums.create', 'jmdmc_forum_admin_forum_create')
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageForumsEdit(Forum $forum)
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-forums.index', 'jmdmc_forum_admin_forum_list')
            ->add(
                array(
                    'label' => 'crumbs.admin.manage-forums.edit',
                    'params' => array(
                        '%forum_name%' => $forum->getName()
                    )
                ),
                array(
                    'route' => 'jmdmc_forum_admin_forum_edit',
                    'params' => array(
                        'forumId' => $forum->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageForumsDelete(Forum $forum)
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-forums.index', 'jmdmc_forum_admin_forum_list')
            ->add(
                array(
                    'label' => 'crumbs.admin.manage-forums.delete',
                    'params' => array(
                        '%forum_name%' => $forum->getName()
                    )
                ),
                array(
                    'route' => 'jmdmc_forum_admin_forum_delete',
                    'params' => array(
                        'forumId' => $forum->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageCategoriesIndex()
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-categories.index', 'jmdmc_forum_admin_category_list')
        ;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageCategoriesCreate()
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-categories.index', 'jmdmc_forum_admin_category_list')
            ->add('crumbs.admin.manage-categories.create', 'jmdmc_forum_admin_category_create')
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Category                     $category
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageCategoriesEdit(Category $category)
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-categories.index', 'jmdmc_forum_admin_category_list')
            ->add(
                array(
                    'label' => 'crumbs.admin.manage-categories.edit',
                    'params' => array(
                        '%category_name%' => $category->getName()
                    )
                ),
                array(
                    'route' => 'jmdmc_forum_admin_category_edit',
                    'params' => array(
                        'categoryId' => $category->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Category                     $category
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageCategoriesDelete(Category $category)
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-categories.index', 'jmdmc_forum_admin_category_list')
            ->add(
                array(
                    'label' => 'crumbs.admin.manage-categories.delete',
                    'params' => array(
                        '%category_name%' => $category->getName()
                    )
                ),
                array(
                    'route' => 'jmdmc_forum_admin_category_delete',
                    'params' => array(
                        'categoryId' => $category->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageBoardsIndex()
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-boards.index', 'jmdmc_forum_admin_board_list')
        ;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageBoardsCreate()
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-boards.index', 'jmdmc_forum_admin_board_list')
            ->add('crumbs.admin.manage-boards.create', 'jmdmc_forum_admin_board_create')
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                        $board
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageBoardsEdit(Board $board)
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-boards.index', 'jmdmc_forum_admin_board_list')
            ->add(
                array(
                    'label' => 'crumbs.admin.manage-boards.edit',
                    'params' => array(
                        '%board_name%' => $board->getName()
                    )
                ),
                array(
                    'route' => 'jmdmc_forum_admin_board_edit',
                    'params' => array(
                        'boardId' => $board->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Board                        $board
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addAdminManageBoardsDelete(Board $board)
    {
        return $this->createCrumbTrail()
            ->add('crumbs.admin.index', 'jmdmc_forum_admin_index')
            ->add('crumbs.admin.manage-boards.index', 'jmdmc_forum_admin_board_list')
            ->add(
                array(
                    'label' => 'crumbs.admin.manage-boards.delete',
                    'params' => array(
                        '%board_name%' => $board->getName()
                    )
                ),
                array(
                    'route' => 'jmdmc_forum_admin_board_delete',
                    'params' => array(
                        'boardId' => $board->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserCategoryIndex(Forum $forum = null)
    {
        return $this->createCrumbTrail()
            ->add(
                $forum ?
                    $forum->getName() == 'default' ?  'Index' : $forum->getName() . ' Index'
                    :
                    'Index'
                ,
                array(
                    'route' => 'jmdmc_forum_user_category_index',
                    'params' => array(
                        'forumName' => $forum ? $forum->getName() : 'default'
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Category                     $category
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserCategoryShow(Forum $forum, Category $category)
    {
        return $this->addUserCategoryIndex($forum)
            ->add(
                $category->getName(),
                array(
                    'route' => 'jmdmc_forum_user_category_show',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'categoryId' => $category->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Board                        $board
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserBoardShow(Forum $forum, Board $board)
    {
        return $this->addUserCategoryShow($forum, $board->getCategory())
            ->add(
                $board->getName(),
                array(
                    'route' => 'jmdmc_forum_user_board_show',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'boardId' => $board->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Topic                        $topic
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserTopicShow(Forum $forum, Topic $topic)
    {
        return $this->addUserBoardShow($forum, $topic->getBoard())
            ->add(
                $topic->getTitle(),
                array(
                    'route' => 'jmdmc_forum_user_topic_show',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'topicId' => $topic->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Board                        $board
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserTopicCreate(Forum $forum, Board $board)
    {
        return $this->addUserBoardShow($forum, $board)
            ->add(
                array(
                    'label' => 'crumbs.user.topic.create',
                ),
                array(
                    'route' => 'jmdmc_forum_user_topic_create',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'boardId' => $board->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Topic                        $topic
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserTopicReply(Forum $forum, Topic $topic)
    {
        return $this->addUserTopicShow($forum, $topic)
            ->add(
                array(
                    'label' => 'crumbs.user.topic.reply',
                ),
                array(
                    'route' => 'jmdmc_forum_user_topic_reply',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'topicId' => $topic->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Post                         $post
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserPostShow(Forum $forum, Post $post)
    {
        return $this->addUserTopicShow($forum, $post->getTopic())
            ->add(
                '# ' . $post->getId(),
                array(
                    'route' => 'jmdmc_forum_user_post_show',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'postId' => $post->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Post                         $post
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserPostDelete(Forum $forum, Post $post)
    {
        return $this->addUserPostShow($forum, $post)
            ->add(
                array(
                    'label' => 'crumbs.user.post.delete',
                ),
                array(
                    'route' => 'jmdmc_forum_user_post_delete',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'postId' => $post->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addUserSubscriptionIndex(Forum $forum)
    {
        return $this->addUserCategoryIndex($forum)
            ->add(
                array(
                    'label' => 'crumbs.user.subscription.index',
                ),
                array(
                    'route' => 'jmdmc_forum_user_subscription_index',
                    'params' => array(
                        'forumName' => $forum->getName()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Topic                        $topic
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addModeratorTopicDelete(Forum $forum, Topic $topic)
    {
        return $this->addUserTopicShow($forum, $topic)
            ->add(
                array(
                    'label' => 'crumbs.moderator.topic.delete',
                ),
                array(
                    'route' => 'jmdmc_forum_moderator_topic_delete',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'topicId'   => $topic->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Topic                        $topic
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addModeratorTopicChangeBoard(Forum $forum, Topic $topic)
    {
        return $this->addUserTopicShow($forum, $topic)
            ->add(
                array(
                    'label' => 'crumbs.moderator.topic.move',
                ),
                array(
                    'route' => 'jmdmc_forum_moderator_topic_change_board',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'topicId'   => $topic->getId()
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Forum                        $forum
     * @param  \JMD\MC\ForumBundle\Entity\Post                         $post
     * @return \JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail
     */
    public function addModeratorPostUnlock(Forum $forum, Post $post)
    {
        return $this->addUserPostShow($forum, $post)
            ->add(
                array(
                    'label' => 'crumbs.moderator.post.unlock',
                ),
                array(
                    'route' => 'jmdmc_forum_moderator_post_unlock',
                    'params' => array(
                        'forumName' => $forum->getName(),
                        'postId'   => $post->getId()
                    )
                )
            )
        ;
    }
}
