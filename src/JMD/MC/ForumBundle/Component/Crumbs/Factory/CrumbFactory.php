<?php
namespace JMD\MC\ForumBundle\Component\Crumbs\Factory;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\RouterInterface;

use JMD\MC\ForumBundle\Component\Crumbs\Factory\CrumbTrail;

class CrumbFactory
{
    /**
     *
     * @var \Symfony\Component\Translation\TranslatorInterface $translator
     */
    private $translator;

    /**
     *
     * @var \Symfony\Component\Routing\RouterInterface $router
     */
    private $router;

    /**
     *
     * @access public
     * @param \Symfony\Component\Translation\TranslatorInterface $translator
     * @param \Symfony\Component\Routing\RouterInterface             $router
     */
    public function __construct(TranslatorInterface $translator, RouterInterface $router)
    {
        $this->translator = $translator;
        $this->router = $router;
    }

    public function createNewCrumbTrail()
    {
        return new CrumbTrail($this->translator, $this->router);
    }
}
