<?php
namespace JMD\MC\ForumBundle\Component\TwigExtension;

use JMD\MC\ForumBundle\Entity\Forum;
use JMD\MC\ForumBundle\Entity\Category;
use JMD\MC\ForumBundle\Entity\Board;
use JMD\MC\ForumBundle\Entity\Topic;
use JMD\MC\ForumBundle\Entity\Post;
use JMD\MC\ForumBundle\Entity\Subscription;

class AuthorizerExtension extends \Twig_Extension
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Component\Security\Authorizer $authorizer
     */
    protected $authorizer;

    /**
     *
     * @access public
     * @param \JMD\MC\ForumBundle\Component\Security\Authorizer $authorizer
     */
    public function __construct($authorizer)
    {
        $this->authorizer = $authorizer;
    }

    /**
     *
     * @access public
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('canShowForum', array($this, 'canShowForum')),
            new \Twig_SimpleFunction('canShowCategory', array($this, 'canShowCategory')),
            new \Twig_SimpleFunction('canShowBoard', array($this, 'canShowBoard')),
            new \Twig_SimpleFunction('canCreateTopicOnBoard', array($this, 'canCreateTopicOnBoard')),
            new \Twig_SimpleFunction('canReplyToTopicOnBoard', array($this, 'canReplyToTopicOnBoard')),
            new \Twig_SimpleFunction('canShowTopic', array($this, 'canShowTopic')),
            new \Twig_SimpleFunction('canReplyToTopic', array($this, 'canReplyToTopic')),
            new \Twig_SimpleFunction('canDeleteTopic', array($this, 'canDeleteTopic')),
            new \Twig_SimpleFunction('canRestoreTopic', array($this, 'canRestoreTopic')),
            new \Twig_SimpleFunction('canCloseTopic', array($this, 'canCloseTopic')),
            new \Twig_SimpleFunction('canReopenTopic', array($this, 'canReopenTopic')),
            new \Twig_SimpleFunction('canTopicChangeBoard', array($this, 'canTopicChangeBoard')),
            new \Twig_SimpleFunction('canStickyTopic', array($this, 'canStickyTopic')),
            new \Twig_SimpleFunction('canUnstickyTopic', array($this, 'canUnstickyTopic')),
            new \Twig_SimpleFunction('canShowPost', array($this, 'canShowPost')),
            new \Twig_SimpleFunction('canEditPost', array($this, 'canEditPost')),
            new \Twig_SimpleFunction('canDeletePost', array($this, 'canDeletePost')),
            new \Twig_SimpleFunction('canRestorePost', array($this, 'canRestorePost')),
            new \Twig_SimpleFunction('canLockPost', array($this, 'canLockPost')),
            new \Twig_SimpleFunction('canUnlockPost', array($this, 'canUnlockPost')),
            new \Twig_SimpleFunction('canSubscribeToTopic', array($this, 'canSubscribeToTopic')),
            new \Twig_SimpleFunction('canUnsubscribeFromTopic', array($this, 'canUnsubscribeFromTopic')),
        );
    }

    public function canShowForum(Forum $forum)
    {
        return $this->authorizer->canShowForum($forum);
    }

    public function canShowCategory(Category $category, Forum $forum = null)
    {
        return $this->authorizer->canShowCategory($category, $forum);
    }

    public function canShowBoard(Board $board, Forum $forum = null)
    {
        return $this->authorizer->canShowBoard($board, $forum);
    }

    public function canCreateTopicOnBoard(Board $board, Forum $forum = null)
    {
        return $this->authorizer->canCreateTopicOnBoard($board, $forum);
    }

    public function canReplyToTopicOnBoard(Board $board, Forum $forum = null)
    {
        return $this->authorizer->canReplyToTopicOnBoard($board, $forum);
    }

    public function canShowTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canShowTopic($topic, $forum);
    }

    public function canReplyToTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canReplyToTopic($topic, $forum);
    }

    public function canDeleteTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canDeleteTopic($topic, $forum);
    }

    public function canRestoreTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canRestoreTopic($topic, $forum);
    }

    public function canCloseTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canCloseTopic($topic, $forum);
    }

    public function canReopenTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canReopenTopic($topic, $forum);
    }

    public function canTopicChangeBoard(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canTopicChangeBoard($topic, $forum);
    }

    public function canStickyTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canStickyTopic($topic, $forum);
    }

    public function canUnstickyTopic(Topic $topic, Forum $forum = null)
    {
        return $this->authorizer->canUnstickyTopic($topic, $forum);
    }

    public function canShowPost(Post $post, Forum $forum = null)
    {
        return $this->authorizer->canShowPost($post, $forum);
    }

    public function canEditPost(Post $post, Forum $forum = null)
    {
        return $this->authorizer->canEditPost($post, $forum);
    }

    public function canDeletePost(Post $post, Forum $forum = null)
    {
        return $this->authorizer->canDeletePost($post, $forum);
    }

    public function canRestorePost(Post $post, Forum $forum = null)
    {
        return $this->authorizer->canRestorePost($post, $forum);
    }

    public function canLockPost(Post $post, Forum $forum = null)
    {
        return $this->authorizer->canLockPost($post, $forum);
    }

    public function canUnlockPost(Post $post, Forum $forum = null)
    {
        return $this->authorizer->canUnlockPost($post, $forum);
    }

    public function canSubscribeToTopic(Topic $topic, Forum $forum = null, Subscription $subscription = null)
    {
        return $this->authorizer->canSubscribeToTopic($topic, $forum, $subscription);
    }

    public function canUnsubscribeFromTopic(Topic $topic, Forum $forum = null, Subscription $subscription = null)
    {
        return $this->authorizer->canUnsubscribeFromTopic($topic, $forum, $subscription);
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return 'authorizer';
    }
}
