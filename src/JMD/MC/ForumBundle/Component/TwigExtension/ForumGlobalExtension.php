<?php
namespace JMD\MC\ForumBundle\Component\TwigExtension;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ForumGlobalExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     *
     * @access protected
     * @var \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    protected $container;

    /**
     *
     * @access public
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     *
     * @access public
     * @return array
     */
    public function getGlobals()
    {
        return array(
            'security'  => $this->container->get('security.authorization_checker'),
            'jmdmc_forum' => array(
                'seo' => array(
                    'title_length' => $this->container->getParameter('jmdmc_forum.seo.title_length'),
                ),
                'forum' => array(
                    'admin' => array(
                        'create' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.forum.admin.create.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.forum.admin.create.form_theme'),
                        ),
                        'delete' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.forum.admin.delete.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.forum.admin.delete.form_theme'),
                        ),
                        'edit' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.forum.admin.edit.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.forum.admin.edit.form_theme'),
                        ),
                        'list' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.forum.admin.list.layout_template'),
                        ),
                    ),
                ),
                'category' => array(
                    'admin' => array(
                        'create' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.category.admin.create.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.category.admin.create.form_theme'),
                        ),
                        'delete' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.category.admin.delete.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.category.admin.delete.form_theme'),
                        ),
                        'edit' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.category.admin.edit.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.category.admin.edit.form_theme'),
                        ),
                        'list' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.category.admin.list.layout_template'),
                        ),
                    ),
                    'user' => array(
                        'last_post_datetime_format' => $this->container->getParameter('jmdmc_forum.category.user.last_post_datetime_format'),
                        'index' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.category.user.index.layout_template'),
                        ),
                        'show' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.category.user.show.layout_template'),
                        ),
                    ),
                ),
                'board' => array(
                    'admin' => array(
                        'create' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.board.admin.create.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.board.admin.create.form_theme'),
                        ),
                        'delete' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.board.admin.delete.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.board.admin.delete.form_theme'),
                        ),
                        'edit' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.board.admin.edit.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.board.admin.edit.form_theme'),
                        ),
                        'list' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.board.admin.list.layout_template'),
                        ),
                    ),
                    'user' => array(
                        'show' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.board.user.show.layout_template'),
                            'topic_title_truncate' => $this->container->getParameter('jmdmc_forum.board.user.show.topic_title_truncate'),
                            'first_post_datetime_format' => $this->container->getParameter('jmdmc_forum.board.user.show.first_post_datetime_format'),
                            'last_post_datetime_format' => $this->container->getParameter('jmdmc_forum.board.user.show.last_post_datetime_format'),
                            'topics_per_page' => $this->container->getParameter('jmdmc_forum.board.user.show.topics_per_page'),
                        ),
                    ),
                ),
                'topic' => array(
                    'moderator' => array(
                        'change_board' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.topic.moderator.change_board.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.topic.moderator.change_board.form_theme'),
                        ),
                        'delete' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.topic.moderator.delete.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.topic.moderator.delete.form_theme'),
                        ),
                    ),
                    'user' => array(
                        'show' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.topic.user.show.layout_template'),
                            'closed_datetime_format' => $this->container->getParameter('jmdmc_forum.topic.user.show.closed_datetime_format'),
                            'deleted_datetime_format' => $this->container->getParameter('jmdmc_forum.topic.user.show.deleted_datetime_format'),
                            'posts_per_page' => $this->container->getParameter('jmdmc_forum.topic.user.show.posts_per_page'),
                        ),
                        'create' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.topic.user.create.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.topic.user.create.form_theme'),
                        ),
                        'reply' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.topic.user.reply.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.topic.user.reply.form_theme'),
                        ),
                    ),
                ),
                'post' => array(
                    'moderator' => array(
                        'unlock' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.post.moderator.unlock.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.post.moderator.unlock.form_theme'),
                        ),
                    ),
                    'user' => array(
                        'show' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.post.user.show.layout_template'),
                        ),
                        'edit' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.post.user.edit.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.post.user.edit.form_theme'),
                        ),
                        'delete' => array(
                            'layout_template' => $this->container->getParameter('jmdmc_forum.post.user.delete.layout_template'),
                            'form_theme' => $this->container->getParameter('jmdmc_forum.post.user.delete.form_theme'),
                        ),
                    ),
                ),
                'item_post' => array(
                    'created_datetime_format' => $this->container->getParameter('jmdmc_forum.item_post.created_datetime_format'),
                    'edited_datetime_format' => $this->container->getParameter('jmdmc_forum.item_post.edited_datetime_format'),
                    'deleted_datetime_format' => $this->container->getParameter('jmdmc_forum.item_post.deleted_datetime_format'),
                ),
                'subscription' => array(
                    'list' => array(
                        'layout_template' => $this->container->getParameter('jmdmc_forum.subscription.list.layout_template'),
                        'topics_per_page' => $this->container->getParameter('jmdmc_forum.subscription.list.topics_per_page'),
                    ),
                ),
            )
        );
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return 'forumGlobal';
    }
}
