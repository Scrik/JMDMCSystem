<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

use JMD\MC\ForumBundle\Entity\Board;

class AdminBoardEvent extends Event
{
    /**
     *
     * @access protected
     * @var \Symfony\Component\HttpFoundation\Request $request
     */
    protected $request;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Board $board
     */
    protected $board;

    /**
     *
     * @access public
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \JMD\MC\ForumBundle\Entity\Board       $board
     */
    public function __construct(Request $request, Board $board = null)
    {
        $this->request = $request;
        $this->board = $board;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     *
     * @access public
     * @return \JMD\MC\ForumBundle\Entity\Board
     */
    public function getBoard()
    {
        return $this->board;
    }
}
