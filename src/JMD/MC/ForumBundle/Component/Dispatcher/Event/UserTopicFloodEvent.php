<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Event;

use Symfony\Component\HttpFoundation\Request;

class UserTopicFloodEvent extends UserTopicEvent
{
    /**
     *
     * @access public
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
