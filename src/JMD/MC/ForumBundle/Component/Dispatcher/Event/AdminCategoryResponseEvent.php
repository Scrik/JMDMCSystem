<?php
namespace JMD\MC\ForumBundle\Component\Dispatcher\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use JMD\MC\ForumBundle\Entity\Category;

class AdminCategoryResponseEvent extends AdminCategoryEvent
{
    /**
     *
     * @access protected
     * @var \Symfony\Component\HttpFoundation\Response $response
     */
    protected $response;

    /**
     *
     * @access public
     * @param \Symfony\Component\HttpFoundation\Request  $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * @param \JMD\MC\ForumBundle\Entity\Category     $category
     */
    public function __construct(Request $request, Response $response, Category $category = null)
    {
        $this->request = $request;
        $this->response = $response;
        $this->category = $category;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}
