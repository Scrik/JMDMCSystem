<?php
namespace JMD\MC\ForumBundle\Component\Helper;

use Symfony\Component\Security\Core\User\UserInterface;

class RoleHelper
{
    /**
     *
     * @access protected
     * @var array $availableRoles
     */
    protected $availableRoles;

    /**
     *
     * @access protected
     * @var array $availableRoleKeys
     */
    protected $availableRoleKeys;

    /**
     *
     * @access public
     * @param array $availableRoles
     */
    public function __construct($availableRoles)
    {

        // default role is array is empty.
        if (empty($availableRoles)) {
            $availableRoles[] = 'ROLE_USER';
        }

        $this->availableRoles = $availableRoles;

        // Remove the associate arrays.
        $this->availableRoleKeys = array_keys($availableRoles);
    }

    /**
     *
     * @access public
     * @return array
     */
    public function getRoleHierarchy()
    {
        $roles = array();

        foreach ($this->availableRoles as $roleName => $roleSubs) {
            $roles[$roleName] = $roleName;
        }

        return $roles;
    }

    /**
     *
     * @access public
     * @return array $availableRoles
     */
    public function getAvailableRoles()
    {
        return $this->availableRoles;
    }

    /**
     *
     * @access public
     * @return array $availableRoles
     */
    public function getAvailableRoleKeys()
    {
        return $this->availableRoleKeys;
    }

    /**
     *
     * @access public
     * @param  \Symfony\Component\Security\Core\User\UserInterface $user
     * @param  string                                              $role
     * @return bool
     */
    public function hasRole(UserInterface $user, $role)
    {
        foreach ($this->availableRoles as $aRoleKey => $aRole) {
            if ($user->hasRole($aRoleKey)) {
                if (in_array($role, $aRole) || $role == $aRoleKey) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * @access public
     * @param  array $userRoles
     * @return int   $highestUsersRoleKey
     */
    public function getUsersHighestRole($usersRoles)
    {
        $usersHighestRoleKey = 0;

        // Compare (A)vailable roles against (U)sers roles.
        foreach ($this->availableRoleKeys as $aRoleKey => $aRole) {
            foreach ($usersRoles as $uRole) {
                if ($uRole == $aRole && $aRoleKey > $usersHighestRoleKey) {
                    $usersHighestRoleKey = $aRoleKey;

                    break; // break because once uRole == aRole we know we cannot match anything else.
                }
            }
        }

        return $usersHighestRoleKey;
    }

    /**
     *
     * @access public
     * @param  array  $userRoles
     * @return string $role
     */
    public function getUsersHighestRoleAsName($usersRoles)
    {
        $usersHighestRoleKey = $this->getUsersHighestRole($usersRoles);

        $roles = $this->availableRoleKeys;

        if (array_key_exists($usersHighestRoleKey, $roles)) {
            return $roles[$usersHighestRoleKey];
        } else {
            return 'ROLE_USER';
        }
    }
}
