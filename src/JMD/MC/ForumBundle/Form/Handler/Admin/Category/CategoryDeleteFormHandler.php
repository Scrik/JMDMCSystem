<?php
namespace JMD\MC\ForumBundle\Form\Handler\Admin\Category;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface ;

use Doctrine\Common\Collections\ArrayCollection;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\AdminCategoryEvent;
use JMD\MC\ForumBundle\Form\Handler\BaseFormHandler;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Category;

class CategoryDeleteFormHandler extends BaseFormHandler
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Form\Type\Admin\Category\CategoryDeleteFormType $categoryDeleteFormType
     */
    protected $categoryDeleteFormType;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\FrontModel\CategoryModel $categoryModel
     */
    protected $categoryModel;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Category $category
     */
    protected $category;

    /**
     *
     * @access public
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface            $dispatcher
     * @param \Symfony\Component\Form\FormFactory                                    $factory
     * @param \JMD\MC\ForumBundle\Form\Type\Admin\Category\CategoryDeleteFormType $categoryDeleteFormType
     * @param \JMD\MC\ForumBundle\Model\FrontModel\CategoryModel                  $categoryModel
     */
    public function __construct(EventDispatcherInterface $dispatcher, FormFactory $factory, $categoryDeleteFormType, ModelInterface $categoryModel)
    {
        $this->dispatcher = $dispatcher;
        $this->factory = $factory;
        $this->categoryDeleteFormType = $categoryDeleteFormType;
        $this->categoryModel = $categoryModel;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Category                                       $category
     * @return \JMD\MC\ForumBundle\Form\Handler\Admin\Category\CategoryDeleteFormHandler
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\Form\Form
     */
    public function getForm()
    {
        if (null == $this->form) {
            if (!is_object($this->category) && !$this->category instanceof Category) {
                throw new \Exception('Category object must be specified to delete.');
            }

            $this->dispatcher->dispatch(ForumEvents::ADMIN_CATEGORY_DELETE_INITIALISE, new AdminCategoryEvent($this->request, $this->category));

            $this->form = $this->factory->create(get_class($this->categoryDeleteFormType), $this->category);
        }

        return $this->form;
    }

    /**
     *
     * @access protected
     * @param \JMD\MC\ForumBundle\Entity\Category $category
     */
    protected function onSuccess(Category $category)
    {
        $this->dispatcher->dispatch(ForumEvents::ADMIN_CATEGORY_DELETE_SUCCESS, new AdminCategoryEvent($this->request, $category));

        if (! $this->form->get('confirm_subordinates')->getData()) {
            $boards = new ArrayCollection($category->getBoards()->toArray());

            $this->categoryModel->reassignBoardsToCategory($boards, null)->flush();
        }

        $this->categoryModel->deleteCategory($category);

        $this->dispatcher->dispatch(ForumEvents::ADMIN_CATEGORY_DELETE_COMPLETE, new AdminCategoryEvent($this->request, $category));
    }
}
