<?php
namespace JMD\MC\ForumBundle\Form\Handler\User\Post;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface ;

use JMD\MC\ForumBundle\Component\Dispatcher\ForumEvents;
use JMD\MC\ForumBundle\Component\Dispatcher\Event\UserPostEvent;
use JMD\MC\ForumBundle\Form\Handler\BaseFormHandler;
use JMD\MC\ForumBundle\Model\FrontModel\ModelInterface;
use JMD\MC\ForumBundle\Entity\Post;

class PostUpdateFormHandler extends BaseFormHandler
{
    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Form\Type\User\Post\PostUpdateFormType $formPostType
     */
    protected $formPostType;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Model\FrontModel\PostModel $postModel
     */
    protected $postModel;

    /**
     *
     * @access protected
     * @var \JMD\MC\ForumBundle\Entity\Post $post
     */
    protected $post;

    /**
     *
     * @access public
     * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface   $dispatcher
     * @param \Symfony\Component\Form\FormFactory                           $factory
     * @param \JMD\MC\ForumBundle\Form\Type\User\Post\PostUpdateFormType $formPostType
     * @param \JMD\MC\ForumBundle\Model\FrontModel\PostModel             $postModel
     */
    public function __construct(EventDispatcherInterface $dispatcher, FormFactory $factory, $formPostType, ModelInterface $postModel)
    {
        $this->dispatcher = $dispatcher;
        $this->factory = $factory;
        $this->formPostType = $formPostType;
        $this->postModel = $postModel;
    }

    /**
     *
     * @access public
     * @param  \JMD\MC\ForumBundle\Entity\Post                                  $post
     * @return \JMD\MC\ForumBundle\Form\Handler\User\Post\PostUpdateFormHandler
     */
    public function setPost(Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     *
     * @access public
     * @return \Symfony\Component\Form\Form
     */
    public function getForm()
    {
        if (null == $this->form) {
            if (! is_object($this->post) || ! ($this->post instanceof Post)) {
                throw new \Exception('Post must be specified to be update a Reply in PostUpdateFormHandler');
            }

            $this->dispatcher->dispatch(ForumEvents::USER_POST_EDIT_INITIALISE, new UserPostEvent($this->request, $this->post));

            $this->form = $this->factory->create(get_class($this->formPostType), $this->post);
        }

        return $this->form;
    }

    /**
     *
     * @access protected
     * @param \JMD\MC\ForumBundle\Entity\Post $post
     */
    protected function onSuccess(Post $post)
    {
        // get the current time, and compare to when the post was made.
        $now = new \DateTime();
        $interval = $now->diff($post->getCreatedDate());

        // if post is less than 15 minutes old, don't add that it was edited.
        if ($interval->format('%i') > 15) {
            $post->setEditedDate(new \DateTime());
            $post->setEditedBy($this->user);
        }

        $this->dispatcher->dispatch(ForumEvents::USER_POST_EDIT_SUCCESS, new UserPostEvent($this->request, $this->post));

        $this->postModel->updatePost($post);

        $this->dispatcher->dispatch(ForumEvents::USER_POST_EDIT_COMPLETE, new UserPostEvent($this->request, $post));
    }
}
