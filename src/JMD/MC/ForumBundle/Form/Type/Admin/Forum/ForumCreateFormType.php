<?php
namespace JMD\MC\ForumBundle\Form\Type\Admin\Forum;

use JMD\MC\ForumBundle\Component\Helper\RoleHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ForumCreateFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $forumClass
     */
    protected $forumClass;

    /**
     *
     * @access protected
     * @var RoleHelper $roleHelper
     */
    protected $roleHelper;

    /**
     *
     * @access public
     * @param string $forumClass
     * @param RoleHelper $roleHelper
     */
    public function __construct($forumClass, RoleHelper $roleHelper)
    {
        $this->forumClass = $forumClass;
        $this->roleHelper = $roleHelper;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                array(
                    'label'              => 'forum.name-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
            ->add('readAuthorisedRoles', ChoiceType::class,
                array(
                    'required'           => false,
                    'expanded'           => true,
                    'multiple'           => true,
                    'choices'            => $this->roleHelper->getRoleHierarchy(),
                    'label'              => 'forum.roles.board-view-label',
                    'translation_domain' => 'JMDMCForumBundle',
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'          => $this->forumClass,
            'csrf_protection'     => true,
            'csrf_field_name'     => '_token',
            // a unique key to help generate the secret token
            'intention'           => 'forum_forum_create_item',
            'validation_groups'   => array('forum_forum_create'),
            'cascade_validation'  => true,
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Forum_ForumCreate';
    }
}
