<?php
namespace JMD\MC\ForumBundle\Form\Type\User\Post;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostCreateFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $postClass
     */
    protected $postClass;

    /**
     *
     * @access public
     * @param string $postClass
     */
    public function __construct($postClass)
    {
        $this->postClass = $postClass;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('body', TextareaType::class,
                array(
                    'label'              => 'post.body-label',
                    'translation_domain' => 'JMDMCForumBundle',
                    'attr'               => [
                        'rows'  => 10
                    ]
                )
            )
            ->add('subscribe', CheckboxType::class,
                array(
                    'mapped'             => false,
                    'required'           => false,
                    'label'              => 'post.subscribe-label',
                    'translation_domain' => 'JMDMCForumBundle',
                    'attr'     => array(
                        'checked' => 'checked'
                    )
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'          => $this->postClass,
            'csrf_protection'     => true,
            'csrf_field_name'     => '_token',
            // a unique key to help generate the secret token
            'intention'           => 'forum_post_create_item',
            'validation_groups'   => array('forum_post_create'),
            'cascade_validation'  => true,
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Post';
    }
}
