<?php
namespace JMD\MC\ForumBundle\Form\Type\User\Topic;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TopicCreateFormType extends AbstractType
{
    /**
     *
     * @access protected
     * @var string $topicClass
     */
    protected $topicClass;

    /**
     *
     * @access public
     * @var string $topicClass
     */
    public function __construct($topicClass)
    {
        $this->topicClass = $topicClass;
    }

    /**
     *
     * @access public
     * @param FormBuilderInterface $builder, array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('board', EntityType::class,
                array(
                    'choice_label'       => 'name',
                    'class'              => 'JMDMCForumBundle:Board',
                    'choices'            => $options['boards'],
                    'label'              => 'board.label',
                    'translation_domain' => 'JMDMCForumBundle',
                    'attr'  => [ 'class'=> 'hidden' ],
                    'label_attr'    => ['class' => 'hidden']
                )
            )
            ->add('title', null,
                array(
                    'label'              => 'topic.title-label',
                    'translation_domain' => 'JMDMCForumBundle'
                )
            )
        ;
    }

    /**
     *
     * @access public
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => $this->topicClass,
            'csrf_protection'    => true,
            'csrf_field_name'    => '_token',
            // a unique key to help generate the secret token
            'intention'          => 'forum_topic_create_item',
            'validation_groups'  => array('forum_topic_create', 'forum_post_create'),
            'boards'             => array(),
        ));
    }

    /**
     *
     * @access public
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'Topic';
    }
}
