<?php
if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'environment.php')) {
    require __DIR__ . DIRECTORY_SEPARATOR . 'environment.php';
}

$container->setParameter('mongodb_uri', empty(getenv('MONGODB_URL')) ? 'mongodb://127.0.0.1:27017/' : getenv('MONGODB_URL'));
$container->setParameter('mongodb_database', empty(getenv('JMD_SYSTEM')) ? 'jmd_system' : getenv('JMD_SYSTEM'));
